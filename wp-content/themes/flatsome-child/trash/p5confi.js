var canvas;
let renderGraphics;

var confiUrl = '../wp-content/themes/flatsome-child/configurator/bin/configurator.glb.xz';
var knifeUrl = '';
//confiUrl = knifeUrl;
let mcx = 0;
let mxy = 0;
let layer = [];
let rotateicon, scaleicon, trashicon;
let currentSide = true;
let scene1 = true;
let sketch = function(p) {
  var img, imageBrowse, imageBtn, textBtn, getVecBtn;
  let UVhull = [];
  let UVcoords = [];
  let UVmedian;


  p.preload = function(){
    rotateicon = p.loadImage('../wp-content/themes/flatsome-child/configurator/img/rotate40x40.png');
    trashicon = p.loadImage('../wp-content/themes/flatsome-child/configurator/img/trash40x40.png');
    scaleicon = p.loadImage('../wp-content/themes/flatsome-child/configurator/img/scale40x40.png');
  }
  p.setup = function(){
    renderGraphics = p.createGraphics(1024,512);
    renderGraphics.imageMode(p.CENTER);
    renderGraphics.blendMode(p.MULTIPLY);

    imageBrowse = p.createFileInput(function(file){
      p.loadImage(file.data, img => {
        layer.push(new Layer(img,layer.length,1));
        for(var i = layer.length-1; i > -1; i--){
          layer[i].deselect();
        }
        layer[layer.length-1].select();
        p.redraw();
      });
    },function(){
      //layer[layer.length-1].select();
    });
    imageBrowse.id("imageBrowse");
    //imageBtn.position(0,210);
    imageBrowse.hide();
    //imageBtn.center('horizontal');
    imageBtn = p.createButton('Bild hinzufügen');
    imageBtn.position(-250, 0);
    imageBtn.size(300,);
    imageBtn.id("imageButton");
    jQuery("#imageButton").click(function () {
      jQuery("#imageBrowse").trigger('click');
    });

    switchSceneBtn = p.createButton('Messer wechseln');
    switchSceneBtn.position(-250, 60);
    switchSceneBtn.size(300,50);
    switchSceneBtn.mousePressed(toggleShop);
    //switchSceneBtn.mousePressed(switchKnife);

    getVecBtn = p.createButton('Vector speichern');
    getVecBtn.position(-250, 120);
    getVecBtn.size(300,50);
    getVecBtn.mousePressed(p.saveSVG);

    switchSideBtn = p.createButton('Seite wechseln');
    switchSideBtn.position(-250, 180);
    switchSideBtn.size(300,50);
    switchSideBtn.mousePressed(function(){
      p.switchSide();
      if(currentSide){
        tweenCamera('FrontPosEmpty', 'Empty', 2, function() {
          console.log("tweensomething?");
        });
      } else {
        tweenCamera('BackPosEmpty', 'Empty', 2, function() {});
      }
    }
    );
    /*
    //jQuery('#imageButton').css("left","0").value("yo");
    textBtn = p.createButton('Text hinzufügen');
    textBtn.position(420, 50);
    textBtn.mousePressed(p.addText);
    switchSceneBtn = p.createButton('Messer wechseln');
    switchSceneBtn.position(420, 80);
    switchSceneBtn.mousePressed(p.switchScene);
    priceBtn = p.createButton('Preis berechnen');
    priceBtn.position(420, 110);
    priceBtn.mousePressed(p.calcPrice);
    */
    canvas = p.createCanvas(1024,512);
    canvas.id('p5canvas');
    //jQuery('#p5canvas').css("width","400px").css("height","200px").css("left","250px").css("top","500px"); //calc(50% - 400px/2)
    jQuery('#p5canvas').css("width","400px").css("height","200px"); //calc(50% - 400px/2)


    init3D(confiUrl);
    //switchScene(confiUrl);

    p.imageMode(p.CENTER);
    //switchScene('https://typemyknife.de/wip2/wp-content/uploads/2020/02/DICK-Premier-Plus-Kochmesser-21-cm.glb.xz');
  }

  p.switchSide = function(){
    for(var i = 0; i < layer.length; i++){
      console.log(layer[i].side);
    }
    currentSide = !currentSide;

    var mask = getMask();
    p.getHull(mask.geometry.attributes.uv.array);

    for(var i = 0; i < layer.length; i++){
      if(layer[i].side == currentSide){
        for(var j = i-1; j > -1; j--){
          layer[j].deselect();
        }
        layer[i].select();
      } else {
        layer[i].deselect();
      }
    }
    console.log(currentSide);
  }
  p.calcPrice = function(){
    let blackPixels = p5render.getBlackPixelCount();
    let klingenlaenge = 21; // !!!
    alert("Schwarzflächenanteil mal Klingenlänge plus 40 .. " + (40+(Math.trunc(blackPixels/20000*klingenlaenge/7.77))));
  }
  p.addText = function(){
    alert("noch nix hier");
  }

  p.saveSVG = function(){
    p.redraw();
    p5render.saveScreenshot();
  }

  //  p.switchScene = function(){
  //  switchKnife(knifeUrl);
  /*
  let url;
  if(scene1){
  url = 'https://www.typemyknife.de/test5.glb.xz';
} else {
url = 'https://www.typemyknife.de/wip2/wp-content/uploads/2020/02/DICK-1778-Chinesisches-Kochmesser.glb.xz';
}
var path = url.replace(/^.*\/\/[^\/]+/, '');
switchScene(path);
scene1 = !scene1;
*/
//}


p.draw = function(){
  //p.background(127);
  //renderGraphics.background(255);

  //p.clear();
  jQuery("#p5canvas")[0].getContext('2d').clearRect(0,0,p.width,p.height); //SLOW AS FUCK! WHY DOES CLEAR NO LONGER WORK?

  if(UVhull != ''){
    renderGraphics.clear();
    renderGraphics.beginShape();
    renderGraphics.noStroke();
    renderGraphics.fill(255);
    for (let point of UVhull) {
      renderGraphics.vertex(point.x, point.y);
    }
    renderGraphics.endShape(p.CLOSE);

    for(var i = 0; i < layer.length; i++){
      if(layer[i].side == currentSide){
        layer[i].renderGfx();
      }
    }

  }


  p5render.graphics(renderGraphics);
  updateMask();
  app.render();

  p.image(renderGraphics,p.width/2,p.height/2);
  p.fill(5,10,0,50);
  p.rect(0,0,p.width,p.height);


  if(UVhull != ''){
    p.beginShape();
    p.noStroke();
    p.fill(0,0,0,80);
    p.vertex(0,0);
    p.vertex(0,p.height);
    p.vertex(p.width,p.height);
    p.vertex(p.width,0);
    p.beginContour();
    for (let point of UVhull) {
      p.vertex(point.x, point.y);
    }
    p.endContour();
    p.endShape(p.CLOSE);


      p5render.UVcutout(UVhull);
  }


  for(var i = 0; i < layer.length; i++){
    if(layer[i].side == currentSide){
      layer[i].renderOverlays();
    }
  }

  p.noLoop();
}




p.mouseDragged = function(){
  if (p.pressedInside && p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0){
    for(var i = 0; i < layer.length; i++){
      if(layer[i].selected){
        if(layer[i].scaling){
          layer[i].scale();
        } else if (layer[i].rotating){
          layer[i].rotate();
        } else {
          layer[i].drag();
        }
        p.redraw();
        return;
      }
    }
  }
}
p.mouseReleased = function(){
  for (var i = 0; i < layer.length; i++){
    layer[i].rotating = false;
    layer[i].dragging = false;
    layer[i].scaling = false;
  }
  //if(cameraSide != currentSide){
  //  p.switchSide();
  //}

  p.pressedInside = false;
  p.redraw();
}
var pressedInside = false;
p.mousePressed = function(){
  if (p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0){
    p.pressedInside = true;
    mcx = p.mouseX;
    mcy = p.mouseY;
    /*
    for(var i = layer.length-1; i > -1; i--){
    if(layer[i].hover() == true && layer[i].selected == false){
    layer[i].select();
    //layer.push(layer.splice(i, 1)[0]);
    return;
  }*/

  if(layer[layer.length-1] != undefined && layer[layer.length-1].selected){
    //console.log("have a selected layer");
  } else {
    //console.log("no selected layer");
  }


  if(p.mouseX > scalex-20 && p.mouseX < scalex+20 && p.mouseY > scaley-20 && p.mouseY < scaley+20){
    layer[selectedLayer].scaling = true;
  }
  if(p.mouseX > rotatex-20 && p.mouseX < rotatex+20 && p.mouseY > rotatey-20 && p.mouseY < rotatey+20){
    layer[selectedLayer].rotating = true;
  }
  if(p.mouseX > trashx-20 && p.mouseX < trashx+20 && p.mouseY > trashy-20 && p.mouseY < trashy+20){
    layer[selectedLayer].trash();
    for(var i = 0; i < layer.length; i++){
      layer[i].layerNumber = i;
    }
  }

  if(layer[selectedLayer] != undefined){
    if(!layer[selectedLayer].scaling && !layer[selectedLayer].rotating){
      for(var i = layer.length-1; i > -1; i--){
        layer[i].deselect();
        if(layer[i].hover()) {
          for(var j = i-1; j > -1; j--){
            layer[j].deselect();
          }
          layer.push(layer.splice(i, 1)[0]);
          layer[layer.length-1].select();
          return;
        }
      }
    }
  }
  p.redraw();
}
}
p.mouseClicked = function(){

}



p.getHull = function(arr){
  var points = [];
  for (let i = 0; i < arr.length; i+=2) {
    points.push(p.createVector(p.map(arr[i],0,1,0,p.width),p.map(arr[i+1],0,1,0,p.height)));
  }
  points.sort((a, b) => a.x - b.x);
  leftOf = function(v1, v2, p) {
    const cross = p5.Vector.sub(v2, v1).cross(p5.Vector.sub(p, v1));
    return cross.z < 0;
  }
  let leftMost = points[0];
  let currentVertex = leftMost;
  UVhull = [];
  UVhull.push(currentVertex);
  let nextVertex = points[1];
  let index = 2;
  let nextIndex = -1;
  UVmedian = p.createVector(0,0);
  while (true){
    let checking = points[index];
    if (checking == undefined){
      if(UVhull != []){
        for(var i = 0; i < UVhull.length; i++){
          UVmedian = p5.Vector.add(UVmedian,UVhull[i]);
        }
        UVmedian.x = UVmedian.x / UVhull.length;
        UVmedian.y = UVmedian.y / UVhull.length;
        let UVx = 0;
        let UVy = 0;
        let UVw = 0;
        let UVh = 0;
        for (let point of UVhull) {
          if(point.x < UVx || UVx == 0) UVx = point.x;
          if(point.y < UVy || UVy == 0) UVy = point.y;
          if(point.x > UVw) UVw == point.x;
          if(point.y > UVh) UVh == point.y;
        }
        UVw -= UVx;
        UVh -= UVy;
      }
      p.redraw();
      return;
    }
    if (leftOf(currentVertex, nextVertex, checking)) {
      nextVertex = checking;
      nextIndex = index;
    }
    index++;
    if (index == points.length && nextVertex != leftMost) {
      UVhull.push(nextVertex);
      currentVertex = nextVertex;
      index = 0;
      points = points.filter((point) => leftOf(currentVertex, leftMost, point));
      nextVertex = leftMost;
    }
  }

}

};
var p5s = new p5(sketch, 'p5container');
