<?php
//session_start();
require_once("functions.php");
if(isset($_GET)){
  $call = array_key_first($_GET);

  if ($call == 'catlist'){
    $product_args = array(
      'status' => 'publish',
      'limit' => -1,
      'category' => $_GET['catlist']
    );
    $products = wc_get_products($product_args);

    $content = '<div id="messer-list" style="padding-top:110px;">';
    foreach ( $products as $product ){
      $image = wp_get_attachment_url( $product->get_image_id() );
      $name = $product->get_title();
      $preis = $product->get_price();
      $link3d = get_field('3d-modell', $product->get_id());
      //$id = $product->get_id();
      $content.='<div class="messer-tile" onclick="alert(\'3d / einzelansicht hier\')">';
      //$thumbnail_id = get_woocommerce_term_meta( $product->term_id, 'thumbnail_id', true );
      $content.='<div class="messer-img-container"><img class="messer-img" src="' . $image . '"></div>';
      $content.='<div class="messer-img-label">' . $name . '<br/><b>' . $preis . ',- €</b></div>';
      $content.='</div>';
    }
    $content .= '</div>';
  }

  else if ($call == 'knife'){
    //$knife = wc_get_product($_GET['knife']);
    //$content = $knife;
    $product = wc_get_product($_GET['knife']);
    $link3d = get_field('3d-modell', $product->get_id());
    $data = array(
      'id' => $_GET['knife'],
      'name' => $product->get_title(),
      'price' => $product->get_price(),
      'beschreibung' => $product->get_description(),
      'link3d' => $link3d
    );
    $content = json_encode($data);
    //  $content = "hi ";
  }

  else if ($call == 'ruf mich an !'){
    $content = "hi ";
  }

  //$page = $content;
  //echo($page);

  echo($content);
}


?>
