<?php
session_start();
require_once("functions.php");
$_SESSION['previous_site'] = $_SESSION['current_site'];
$_SESSION['current_site'] = $_SERVER['QUERY_STRING'];
if(isset($_GET)){
  $pagename = array_key_first($_GET);


  if ($pagename == 'hersteller'){
    $herstellertiles = '<div class="hersteller-grid">';
    $hersteller = wc_subcats_from('Hersteller');
    foreach($hersteller as $marke){
      $serien = wc_subcats_from($marke->name);
      $url = get_category_link($marke);
      $herstellertiles.='<div class="prod-tile" onclick="loadSite(\'marke=' . $marke->term_id . '\')">';
      $thumbnail_id = get_woocommerce_term_meta( $marke->term_id, 'thumbnail_id', true );
      $image = wp_get_attachment_url( $thumbnail_id );
      $herstellertiles.='<div class="prod-img-container"><img class="thumb-hersteller" src="' . $image . '"></div>';
      $herstellertiles.='</div>';
    }
    $herstellertiles .= '</div>';
    preg_replace( "/\r|\n/", "", $herstellertiles );
    $headline = 'Hersteller';
    $navi = '<ul><li><a href="#" onclick="loadSite(\'types\')">Typen</a></li><li><a href="#" onclick="loadSite(\'3d\')">Produkte mit 3D-Modell</a></li></ul>';
    $content = $herstellertiles;
  }


  else if ($pagename == 'marke'){
    if (isset($_GET['marke'])){
      $marke =  get_the_category_by_ID($_GET['marke']);
      $serien = wc_subcats_from_ID($_GET['marke']);
      $serientiles = '<div id="serien-list">';
      foreach($serien as $serie){
        $serientiles.='<div class="serien-tile" onclick="loadSite(\'serie=' . $serie->term_id . '\')">';
        $thumbnail_id = get_woocommerce_term_meta( $serie->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        $serientiles.='<div class="serie-img-container"><img class="serie-img" src="' . $image . '"></div>';
        $serientiles.='<div class="serie-img-label">' . $serie->name . '</div>';
        $serientiles.='</div>';
      }
      $headline = $marke;
      $navi = '<a href="#" onclick="loadSite(\'hersteller\')">Zurück</a>';
      $content = $serientiles;
      $serientiles .= '</div>';
    }
  }


  else if ($pagename == 'serie'){
    //echo('<script>alert("' . $_GET['sid'] . '");</script>');
    $serie = get_the_category_by_ID($_GET['serie']);
    $product_args = array(
      'status' => 'publish',
      'limit' => -1,
      'category' => $serie
    );
    $products = wc_get_products($product_args);
    $messertiles = '<div id="messer-list">';
    foreach($products as $product){
      $messertiles.='<div class="messer-tile" onclick="alert(\'3d / einzelansicht hier\')">';
      //$thumbnail_id = get_woocommerce_term_meta( $product->term_id, 'thumbnail_id', true );
      $image = wp_get_attachment_url( $product->get_image_id() );
      $messertiles.='<div class="messer-img-container"><img class="messer-img" src="' . $image . '"></div>';
      $messertiles.='<div class="messer-img-label">' . $product->name . '<br/><b>' . $product->price . ',- €</b></div>';
      $messertiles.='</div>';
    }
    $messertiles .= '</div>';


    /*echo '<p>';
    echo 'Name: ' . $name . '<br>'; // Product title
    echo 'Preis: ' . $preis . '<br>';          // Product price
    echo 'Link Thumbnail: ' . $image . '<br>';
    echo 'Link 3D-Objekt: ' . $link3d . '<br>';
    echo 'ID: '    . $id;    // Product ID
    echo '</p>';*/

    /*
    foreach ( $products as $product ){
    $thumbnail_id   = get_post_thumbnail_id($product->get_id(), 'thumbnail_id', true);
    //$image          = wp_get_attachment_image_url( $thumbnail_id ); //thumbnail nicht korrekte wahl, quadratisch, problem..
    $image          = wp_get_attachment_url( $product->get_image_id() );;
    $link3d = get_field('3d-modell', $product->get_id());
    $name = $product->get_title();
    $preis = $product->get_price();
    $id = $product->get_id();
    $messertiles .= '<div class="messer tile"><span>' . $name . '</span><br><img src="' . $image . '"></img></div>';
  }
  */

  $headline = $serie;
  $navi = '<a href="#" onclick="loadSite(\'' . $_SESSION['previous_site'] . '\');">Zurück</a>';
  $content = $messertiles;
}




else if ($pagename == 'types'){
  //$headline = $serie;
  $headline = "Typen";
  $navi = '<a href="#" onclick="loadSite(\'' . $_SESSION['previous_site'] . '\');">Zurück</a>';
  $content = '<div id="verwendungen-list"></div>';
  $content .= '<script>';
  $content .= 'jQuery(\'.verwendungen-slider\').prependTo(\'#overlay-content\');';
  $content .= 'jQuery(".verwendungen-slider").fadeIn("fast");';
  $content .= 'var activeUse =  jQuery(\'.slider\').find(\'div[aria-selected="true"]\').find(\'.caption\').text();';
  $content .= "loadList('category', activeUse);";
  $content .= '</script>';

}




else if ($pagename == '3d'){
  $product_args = array(
    'status' => 'publish',
    'limit' => -1,
    'category' => 'Verwendung'
  );
  $products = wc_get_products($product_args);




  $messertiles = '<div id="messer-list">';
  foreach ( $products as $product ){
    $link3d = get_field('3d-modell', $product->get_id());
    if($link3d != ''){
      $image          = wp_get_attachment_url( $product->get_image_id() );
      $name = $product->get_title();
      $preis = $product->get_price();
      $id = $product->get_id();
      $messertiles.='<div class="messer-tile" onclick="switchKnife(\'' . $id . '\')">';
      $messertiles.='<div class="messer-img-container"><img class="messer-img" src="' . $image . '"></div>';
      $messertiles.='<div class="messer-img-label">' . $name . '<br/><b>' . $preis . ',- €</b></div>';
      $messertiles.='</div>';
    }
  }
  $messertiles .= '</div>';



  $headline = "Produkte in 3D";
  $navi = '<a href="#" onclick="loadSite(\'' . $_SESSION['previous_site'] . '\');">Zurück</a>';
  $content = $messertiles;



} else {
  exit;
}


$page = '';
$page .= '    <div id="overlay-frame">';
$page .= '         <div id="overlay-left">';
$page .= '           <h2 id="overlay-headline">';
$page .= $headline;
$page .= '           </h2>';
$page .= '           <div id="overlay-navi">';
$page .= $navi;
$page .= '           </div>';
$page .= '         </div>';
$page .= '         <div id="overlay-center">';
$page .= '           <div id="overlay-content">';
$page .= $content;
$page .= '           </div>';
$page .= '         </div>';
$page .= '       </div>';

echo($page);
}




?>
