'use strict';

var ctxSettings = {};
ctxSettings.alpha = true;
ctxSettings.preserveDrawingBuffer = true;
//var preloader = createCustomPreloader(initOptions.preloaderProgressCb,initOptions.preloaderEndCb);
//var preloader = new v3d.SimplePreloader({ container: 'v3dcontainer', imageURL:'drache1.jpg', imageRotationSpeed: 0 });

var app = new v3d.App('v3dcontainer', ctxSettings, null); // <- preloader statt null


////app.clearBkgOnLoad = true;
//app.renderer.setClearColor(0x000000, 0); //transparency!
//app.renderCallbacks.push(updateMask);

function matGetEditableTextures(matName) {
  var mats = [v3d.SceneUtils.getMaterialByName(app, matName)];
  var textures = mats.reduce(function(texArray, mat) {
    var matTextures = Object.values(mat.nodeTextures);
    Array.prototype.push.apply(texArray, matTextures);
    return texArray;
  }, []);
  return textures.filter(function(elem) {
    return elem && (elem.constructor == v3d.Texture || elem.constructor == v3d.DataTexture);
  });
}

function updateMask() {
  if(app.scene && knifeUrl != ''){
    var tex;
    if(currentSide){
      tex = matGetEditableTextures("mask")[0];
    } else {
      tex = matGetEditableTextures("mask_back")[0];
    }
    tex.image = document.getElementById('rendercanvas');
    tex.format = v3d.RGBFormat;
    tex.needsUpdate = true;
  }
}

function getObjByName(objName){
  var objFound;
  app.scene.traverse(function(obj) {
    if (!objFound && (obj.name == objName)) {
      objFound = obj;
    }
  });
  console.log("object found: " + objFound);
  return objFound;
}

function tweenCamera(posObjName, targetObjName, duration, doSlot) {

  console.log("tweening");
  duration = Math.max(0, duration);

  if (!targetObjName)
  return;
  console.log("target: " + targetObjName);
  if (posObjName)
  var posObj = getObjByName(posObjName);
  else
  var posObj = app.camera;
  console.log("habe ich position? " + posObj);
  var targetObj = getObjByName(targetObjName);
  if (!posObj || !targetObj)
  return;

  var wPos = posObj.getWorldPosition();
  var wTarget = targetObj.getWorldPosition();
  if (app.controls && app.controls.tween) {
    // orbit and flying cameras
    if (!app.controls.inTween)
    console.log("controls passen, renne tween mit wPos: " + wPos + " wTarget: " + wTarget + " duration: " + duration + " doSlot..");
    app.controls.tween(wPos, wTarget, duration, doSlot);
  } else { // TODO: static camera, just position it for now
    if (app.camera.parent)
    app.camera.parent.worldToLocal(wPos);
    app.camera.position.copy(wPos);
    app.camera.lookAt(wTarget);
    doSlot();
  }
}

function init3D(url){
  app.loadScene(url, function() {
    app.enableControls();
    app.animate = function(){
      var scope = this;


      var cb = function() {
        requestAnimationFrame(cb);

        var elapsed = scope.clock.getDelta();
        scope.elapsed = elapsed;

        if (scope.controls)
        scope.controls.update(elapsed);
      }

      cb();
    }
    app.run();
    //loadKnife();
    runCode();
    //dof(0.0,200, 0.025, 1);
    //bloom(7.5, 0.1, 0.5);
    /* CALL P5 HERE*/
  });

  console.log(app);
}

var getMask = function(){
  const appendedScene = app.scene.children.find(attr => attr.name == 'Scene');
  var mask;
  if(currentSide){
    mask = appendedScene.children.filter(attr => attr.name == 'mask');
  } else {
    mask = appendedScene.children.filter(attr => attr.name == 'mask_back');
  }
  return mask[0];

}

var knife;
function loadKnife(id) {
  knife = '';
  console.log(shopOpen);
  if(shopOpen) toggleShop();
  jQuery.ajax({
    url: '../wp-content/themes/flatsome-child/overlay-catalogue/sites/blocks.php?knife=' + id,
    type: 'post',
    data: {id:id},
    success: function (response) {


      knife = JSON.parse(response);
      console.log(knife);

      if(!(knife.link3d === knifeUrl)){
        if(knifeUrl != '') unloadKnife();
        knifeUrl = knife.link3d;
        app.appendScene (knifeUrl, function(){
          jQuery('#name h1').html(knife.name);
          var mask = getMask();
          p5s.getHull(mask.geometry.attributes.uv.array);
        }, null, null, false, false);
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus, errorThrown);
    }
  });


  /*jQuery('#verwendungen-list').load('../wp-content/themes/flatsome-child/overlay-catalogue/sites/blocks.php?knife=' + id, function(){
  console.log("finished loading knife.");
});*/


//knife3dlink


//knifeUrl = url;

}



function unloadKnife(){
  //knifeUrl = '';
  const appendedScene = app.scene.children.find(attr => attr.name == 'Scene');
  console.log(appendedScene);
  var appendedMeshs = appendedScene.children.filter(attr => attr.type == 'Mesh');
  const appendedGroups = appendedScene.children.filter(attr => attr.type == 'Group');
  appendedGroups.forEach(function(group){
    var meshingroup = group.children.filter(attr => attr.type == 'Mesh');
    meshingroup.forEach(function(mesh){
      appendedScene.remove(mesh);
      mesh.geometry.dispose();
      mesh.material.dispose();
      mesh = undefined;

    });
  });
  appendedMeshs.forEach(function(mesh, i, arr){

    appendedScene.remove(mesh);
    mesh.geometry.dispose();
    mesh.material.dispose();
    mesh = undefined;
  });
  app.scene.remove(appendedScene);
  var index = app.scene.children.indexOf(appendedScene);
  app.scene.children.splice(1,index);
  appendedScene.dispose();
}

function switchKnife(id){
  //animation ...
  if(shopOpen){
    toggleShop();
  }

  loadKnife(id);
}

var cameraSide;
function runCode(){
  // render the scene first time immediately after loading
  app.render();

  // use the 'change' event of the application controls, to render the scene
  // each time the camera moves
  if (app.controls) {
    app.controls.addEventListener('change', function() {
      cameraSide = (app.controls.object.position.z > 0);
      if (!app.controls.inTween){
        if(cameraSide != currentSide){
          p5s.switchSide();
        }
      }
      app.render();
    });
  }


  // viewport resizing changes the camera's aspect; re-render the scene
  window.addEventListener('resize', function() {
    app.render();
  }, false);

  //console.log(app);
}

function dof(focus, aperture, maxblur, depthLeakThreshold) {
  app.enablePostprocessing([{
    type: 'dof',
    focus: focus,
    aperture: aperture,
    maxblur: maxblur,
    depthLeakThreshold: depthLeakThreshold
  }]);
}
function bloom(threshold, strength, radius) {
  app.enablePostprocessing([{
    type: 'bloom',
    threshold: threshold,
    strength: strength,
    radius: radius
  }]);
}
