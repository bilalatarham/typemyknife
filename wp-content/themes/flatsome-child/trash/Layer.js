class Layer {
  constructor(image, layerno, layermode) {
    this.mode = layermode;
    this.side = currentSide;

    this.layerNumber = layerno;
    this.selected = false;
    this.image = image;

    this.x = p5s.width/2;
    this.y = p5s.height/2;
    this.aspectR = this.image.width/this.image.height;

    this.w = 700;
    this.h =  this.w/this.aspectR;
    while(this.h > p5s.height || this.w > p5s.width){
      this.w--;
      this.h =  this.w/this.aspectR;
    }

    switch (this.mode){
      case 0:
      console.log("layer with layermode 0");
      break;
      case 1:
      console.log("layer with layermode 1");
      break;
      case 2:
      console.log("layer with layermode 2");
      break;
      default:
      console.log("layer with unkown layermode");
      break;
    }


    this.slider = p5s.createSlider(10,90,50,1);
    this.slider.style('width', '300px');
    this.slider.position(0,210);
    this.slider.center('horizontal');
    this.slider.input(function(){
      p5s.redraw();
    });

    this.dragging = false;
    this.rotating = false;
    this.scaling = false;
  }

  hover(){
    if(currentSide == this.side && p5s.mouseX > this.x - this.w/2 && p5s.mouseX < this.x + this.w/2 && p5s.mouseY > this.y - this.h/2 && p5s.mouseY < this.y + this.h/2){
      return true;
    } else {
      return false;
    }
  }

  select(){
    this.selected = true;
    this.slider.show();
    this.updateButtonCoords();
    for(var i = 0; i < layer.length; i++){
      layer[i].layerNumber = i;
    }
    selectedLayer = layer.indexOf(this);
  }

  deselect(){
    this.selected = false;
    this.slider.hide();
    this.rotating = false;
    this.scaling = false;
    this.dragging = false;
  }



  updateButtonCoords(){
    trashx = this.x + this.w/2;
    trashy = this.y - this.h/2;
    rotatex = this.x - this.w/2;
    rotatey = this.y + this.h/2;
    scalex = this.x - this.w/2;
    scaley = this.y - this.h/2;
  }

  drag(){
    this.x += p5s.mouseX - mcx;
    this.y += p5s.mouseY - mcy;
    mcx = p5s.mouseX;
    mcy = p5s.mouseY;
    this.updateButtonCoords();
  }
  scale(){
    console.log("scaling...");
    //var scale = 1;
    /*
    this.w -= (p5s.mouseX - mcx)*2;
    this.h -= (p5s.mouseY - mcy)*2;
    mcx = p5s.mouseX;
    mcy = p5s.mouseY;*/

    var changeFactor = (p5s.mouseX + p5s.mouseY) - (mcx + mcy);
    this.w -= changeFactor * this.aspectR;
    this.h -= changeFactor;
    mcx = p5s.mouseX;
    mcy = p5s.mouseY;

    this.updateButtonCoords();
  }
  rotate(){
    console.log("rotating...");


    this.updateButtonCoords();
  }


  trash(){
    this.deselect();
    layer.splice(selectedLayer,1);
    if(layer[layer.length-1] != undefined){
      layer[layer.length-1].select();
    }

  }

  renderGfx(){
    renderGraphics.image(this.image,this.x,this.y,this.w-6,this.h-6);
    renderGraphics.filter(p5s.THRESHOLD,this.slider.value()/100);
  }

  renderOverlays() {
    if(this.selected){
      this.updateButtonCoords();
      p5s.noFill();
      p5s.stroke(230,230,230);
      p5s.strokeWeight(4);
      p5s.rect(this.x - this.w/2, this.y - this.h/2,this.w,this.h);
      p5s.fill(50,50,50);
      p5s.noStroke();
      p5s.rect(this.x - this.w/2-20,this.y - this.h/2-20,40,40);
      p5s.image(scaleicon,scalex,scaley);

      p5s.rect(this.x + this.w/2-20,this.y - this.h/2-20,40,40);
      p5s.image(trashicon,trashx,trashy);

      //p5s.rect(this.x - this.w/2-20,this.y + this.h/2-20,40,40);
      //p5s.image(rotateicon,rotatex,rotatey);
    }
  }

}
let selectedLayer;
let trashx, trashy;
let rotatex, rotatey;
let scalex, scaley;
let iconSize = 40;
