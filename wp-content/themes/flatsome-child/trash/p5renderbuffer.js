
let rendersketch = function(p){
  let rendercanvas;
  p.setup = function(){
    rendercanvas = p.createCanvas(2048,1024);
    rendercanvas.id('rendercanvas');
    jQuery('#rendercanvas').hide();
    //jQuery('#rendercanvas').css("left","500px");
    p.noLoop();
  }
  p.graphics = function(gfx){
    p.image(gfx,0,0,p.width,p.height);
  }
  p.UVcutout = function(UVhull){

    p.beginShape();
    p.noStroke();
    p.fill(255,255,255);
    p.vertex(0,0);
    p.vertex(0,p.height);
    p.vertex(p.width,p.height);
    p.vertex(p.width,0);
    p.beginContour();
    for (let point of UVhull) {
      p.vertex(point.x*2, point.y*2);
    }
    p.endContour();
    p.endShape(p.CLOSE);

    /*
    if(serverScrs){
    //p.filter(p.BLUR,2);
    var canvas = jQuery('#rendercanvas')[0];
    var data = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
    let iname = "screenshot.png"
    jQuery.post('https://www.coalmedia.eu/knife/save.php' ,{data: data, iname});
    window.open("https://www.coalmedia.eu/knife/converter.php","_blank","menubar=1,resizable=1,width=350,height=250");
    serverScrs = false;
  }*/
}


p.saveScreenshot = function(){
  //p.filter(p.BLUR,2);
  var canvas = jQuery('#rendercanvas')[0];
  var data = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
  let iname = "screenshot.png"
  jQuery.post('https://www.coalmedia.eu/knife/save.php' ,{data: data, iname});
  window.open("https://www.coalmedia.eu/knife/converter.php","_blank","menubar=1,resizable=1,width=350,height=250");

  //serverScrs = true;
}

//let serverScrs = false;

p.keyPressed = function(){
  if (p.keyCode === p.UP_ARROW) {
    //serverScrs = true;
  }
}

p.getBlackPixelCount = function(){
  let blackPixelCount = 0;
  p.loadPixels();
  for (let i = 0; i < p.width; i++) {
    for (let j = 0; j < p.height; j++) {
      if(p.get(i,j)[0] < 127){
        blackPixelCount++;
      }
    }
  }
  p.updatePixels();


  return(blackPixelCount)
}
}
var p5render = new p5(rendersketch, 'rendercontainer');
