<?php

//wp_localize_script('mylib', 'WPURLS', array( 'siteurl' => get_option('siteurl') ));
function additional_scripts() {

    wp_enqueue_script('custom_script', get_theme_file_uri( 'script.js'));
    wp_enqueue_script('dom-to-image', get_theme_file_uri( 'domtoimage.js'));
    

    wp_enqueue_style( 'style', get_theme_file_uri( '/style.css'));
  //wp_enqueue_style( 'overlay-style', get_theme_file_uri( '/overlay-catalogue/style.css'));
  if ( is_page('konfigurator') ) {
    wp_enqueue_style( 'confi-style', get_theme_file_uri( '/configurator/style.css'));
    wp_enqueue_style( 'fontawesone-style', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

    wp_enqueue_script('v3d', get_theme_file_uri( '/configurator/libs/v3d.js'));
    wp_enqueue_script('p5', get_theme_file_uri( '/configurator/libs/p5.min.js'));
    
    wp_enqueue_script('SVG', get_theme_file_uri( '/configurator/libs/p5.svg.js'));
    //wp_enqueue_script('pdf', get_theme_file_uri( '/configurator/libs/p5.pdf.js'));
   
    wp_enqueue_script('p5dom', get_theme_file_uri( '/configurator/libs/p5.dom.min.js'));
    wp_enqueue_script('p5sound', get_theme_file_uri( '/configurator/libs/p5.sound.min.js'));

    wp_enqueue_script( 'v3d_add_cart', get_theme_file_uri( '/configurator/cart.js'));
    wp_localize_script( 'v3d_add_cart', 've3_product_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ),'we_value' => 1234));
    wp_enqueue_script( 'v3d_add_cart' );
  

    


    //wp_enqueue_script('confi-functions', get_theme_file_uri( '/configurator/functions.js',false,true));

    //wp_enqueue_script('v3dscripts', get_theme_file_uri( '/configurator/v3dscripts.js'),array(),false,true);
    //wp_enqueue_script('Layer', get_theme_file_uri( '/configurator/Layer.js'),array(),false,true);
    //wp_enqueue_script('p5confi', get_theme_file_uri( '/configurator/p5confi.js'),array(),false,true);
    //wp_enqueue_script('p5renderbuffer', get_theme_file_uri( '/configurator/p5renderbuffer.js'),array(),false,true);
    //wp_enqueue_script('scripts-bottom', get_theme_file_uri( '/configurator/scripts-bottom.js'),array(),false,true);
  }
}
add_action('wp_enqueue_scripts', 'additional_scripts');


function addto_content($content) {
   
    
    if ( is_page('konfigurator') )
    {
        // $configuratorLink = get_post_meta( $_GET['prod_id'] ,'v3d_knife_configurator_enable', true);
        // echo $configuratorLink;
        // exit("aaaaaaaaaaaaaaaaaa");
        // if($configuratorLink)
        // {
            $configurator = file_get_contents(dirname( __FILE__ ) . '/configurator/configurator.htm');
           $script = '<script src="'.get_theme_file_uri( '/configurator/knife.js').'"></script>';
           $script .= '<script src="'.get_theme_file_uri( '/configurator/viewport.js').'"></script>';
           $script .= '<script src="'.get_theme_file_uri( '/configurator/gui.js').'"></script>';
           $script .= '<script src="'. get_theme_file_uri( '/configurator/main-menu.js').'"></script>';
          

           $configurator = str_replace('<div id="script-s"></div>',$script,$configurator);
            $content .= $configurator;
        // }
        // else
        // {
        //     wp_redirect('shop');
        // }
    }
    else
    {
        $content = $content;
    }
    return $content;
}
add_filter('the_content', 'addto_content');


//Exclude products from a particular category on the shop page
function custom_pre_get_posts_query( $q ) {
  $tax_query = (array) $q->get( 'tax_query' );
  $tax_query[] = array(
    'taxonomy' => 'product_cat',
    'field' => 'slug',
    'terms' => array( 'verpackung' ), // Don't display products in the clothing category on the shop page.
    'operator' => 'NOT IN'
  );
  $q->set( 'tax_query', $tax_query );
}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );


function tmk_custom_upload_mimes($mimes = array()) {

    $mimes['gltf'] = "model/gltf+json";
    $mimes['bin'] = "model/gltf-binary";
    $mimes['glb'] = "model/gltf-binary";
    $mimes['gltf.xz'] = "model/gltf+json";
    $mimes['bin.xz'] = "model/gltf-binary";
    $mimes['glb.xz'] = "model/gltf-binary";
    $mimes['xz'] = "application/x-xz";

    return $mimes;
}

add_action('upload_mimes', 'tmk_custom_upload_mimes');


add_action( 'wp_ajax_save_vector_file', 'save_vector_file' );
add_action( 'wp_ajax_nopriv_save_vector_file', 'save_vector_file' );

//add_action( 'wp_ajax_send_subscribe_email', '' );
function save_vector_file() {

    $uploads   = wp_upload_dir();
    $filename = '';
    $secfilename = '';
    session_start();

    if(isset($_POST['frontvector']) && $_POST['one_side_price_cart'] > 0 )
    {

        $filename = $uploads['basedir'].'/tmkconfigurator/'.$_POST['guid'].'one.jpeg';
        $filesvg = $uploads['basedir'].'/tmkconfigurator/'.$_POST['guid'].'one.svg';
        $content  = $_POST['frontvector'];//stripslashes($_POST['frontvector']);


        

        // $imageContent = file_get_contents($content);
        $base_to_php = explode(',', $content);
        $imageContent = base64_decode($base_to_php[1]);
       

   


        file_put_contents($filename,$imageContent);
        file_put_contents($filesvg,svg_format($content));
        $filename = str_replace( $uploads['basedir'], $uploads['baseurl'], $filename );
        $filesvg = str_replace( $uploads['basedir'], $uploads['baseurl'], $filesvg );
        
        $_SESSION['frontvector_'.$_POST['product_id']] = $filename;
        $_SESSION['frontsvg_'.$_POST['product_id']] = $filesvg;
        $_SESSION['one_side_price_cart_'.$_POST['product_id']] = $_POST['one_side_price_cart'];
    }
    else
    {
        unset($_SESSION['frontvector_'.$_POST['product_id']]);
        unset($_SESSION['one_side_price_cart_'.$_POST['product_id']]);
        // unset($_SESSION['frontsvg_'.$_POST['product_id']]);
    }
    
    if(isset($_POST['backendvector']) && $_POST['sec_side_price_cart'] > 0)
    {
        $backContent  = $_POST['backendvector'];//stripslashes($_POST['backendvector']);
      
        $secfilename = $uploads['basedir'].'/tmkconfigurator/'.$_POST['guid'].'two.jpeg';
        $secfilesvg = $uploads['basedir'].'/tmkconfigurator/'.$_POST['guid'].'two.svg';
        // $imageBackContent = file_get_contents($backContent);
        $base_to_php = explode(',', $backContent);
        $imageBackContent = base64_decode($base_to_php[1]);
        file_put_contents($secfilename,$imageBackContent);
        file_put_contents($secfilesvg,svg_format($backContent));
        $secfilename = str_replace( $uploads['basedir'], $uploads['baseurl'], $secfilename);
        $secfilesvg = str_replace( $uploads['basedir'], $uploads['baseurl'], $secfilesvg );

        $_SESSION['backendvector'.$_POST['product_id']] = $secfilename;
        $_SESSION['backsvg_'.$_POST['product_id']] = $secfilesvg;
        $_SESSION['sec_side_price_cart_'.$_POST['product_id']] = $_POST['sec_side_price_cart'];
    }
    else
    {
        unset($_SESSION['backendvector'.$_POST['product_id']]);
        unset($_SESSION['sec_side_price_cart_'.$_POST['product_id']]);
    }

    //save pdf file
    savePDF($_POST['guid'],$filesvg,$secfilesvg,$_POST['product_id']);
}

function savePDF($pdfFileName,$filename,$secfilename,$product_id)
{
    // create new PDF document
    $pdf = new  TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Arhamsoft');
    $pdf->SetTitle('TCPDF Example 002');
    $pdf->SetSubject('Designed Knife');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // add a page
    $pdf->AddPage();

    if (($filename)) 
    {
        $pdf->ImageSVG($filename, $x=15, $y=30, $w='', $h='', $link='', $align='', $palign='', $border=1, $fitonpage=true);
    }

    if ($secfilename) 
    {
        $pdf->AddPage();
        $pdf->ImageSVG($secfilename, $x=30, $y=100, $w='', $h=100, $link='', $align='', $palign='', $border=0, $fitonpage=true);
    }
    
    $uploads   = wp_upload_dir();
    $pdfFile = $uploads['basedir'].'/tmkconfigurator/pdf/'.$pdfFileName.'.pdf';
    $pdfFileRelative = str_replace( $uploads['basedir'], $uploads['baseurl'], $pdfFile);

    session_start();
    if(isset($_SESSION['pdftvector_'.$product_id]))
    {
        unset($_SESSION['pdftvector_'.$product_id]);
    }
    $_SESSION['pdftvector_'.$product_id] = $pdfFileRelative;

    if (file_exists($pdfFile)) {
        unlink($pdfFile);
    }
    //$pdf->Output($pdfFile, 'F');
    $pdf->Output($pdfFile, 'F');
}


//add to cart product
add_action( 'wp_ajax_get_knife_price', 'get_knife_price' );
add_action( 'wp_ajax_nopriv_get_knife_price', 'get_knife_price' );

//add_action( 'wp_ajax_send_subscribe_email', '' );
function get_knife_price() {

    //$option = get_option($args[0]);
    $onc_side = get_option('one_side_price');
    $sec_side = get_option('second_side_price');
    

    //$onc_side = get_post_meta( $_POST['product_id'],'v3d_knife_one_side_price', true);
    //$sec_side = get_post_meta( $_POST['product_id'],'v3d_knife_second_side_price', true);
    echo json_encode(array('one_side' => $onc_side, 'sec_side' => $sec_side));
    exit;
}

//add to cart product
add_action( 'wp_ajax_send_subscribe_email', 'send_subscribe_email' );
add_action( 'wp_ajax_nopriv_send_subscribe_email', 'send_subscribe_email' );

//add_action( 'wp_ajax_send_subscribe_email', '' );
function send_subscribe_email() {

    global $woocommerce;
    $post = $_POST;
    $qty = 1;
    //$cart_item_data = ['price_prod' => $post['price_prod'],'quantity_prod' => $post['quantity_prod']];
    //$woocommerce->cart->add_to_cart($post['product_id'],$qty,'',[],$cart_item_data);
    $woocommerce->cart->add_to_cart($post['product_id'],$qty);
}


//Step 1:-need to create some custom hidden fields to send custom data that will show on single product page. for example :-
// add_action('woocommerce_before_add_to_cart_button', 'custom_data_hidden_fields');
// function custom_data_hidden_fields() {
//     echo '<div class="imput_fields custom-imput-fields">
//         <label class="price_prod">price: <br><input type="text" id="price_prod" name="price_prod" value="300" /></label>
//         <label class="quantity_prod">quantity: <br>
//             <select name="quantity_prod" id="quantity_prod">
//                 <option value="1">1</option>
//                 <option value="2">2</option>
//                 <option value="3" selected="selected">3</option>
//                 <option value="4">4</option>
//                 <option value="5">5</option>
//             </select>
//         </label>
//     </div><br>';
// }

//Step 2:- Now after done that you need to write the main logic for Save all Products custom fields to your cart item data, follow the below codes.

// Logic to Save products custom fields values into the cart item data
add_action( 'woocommerce_add_cart_item_data', 'save_custom_data_hidden_fields', 10, 2 );
function save_custom_data_hidden_fields( $cart_item_data, $product_id ) {
 
    session_start();
    
    $data = array();
    if( isset( $_REQUEST['price_prod'] ) && isset($_REQUEST['update_price_flag']) ) {
        $cart_item_data['custom_data']['price_prod'] = $_REQUEST['price_prod'];
        $data['price_prod'] = $_REQUEST['price_prod'];
    }

    if( isset( $_REQUEST['quantity_prod'] ) && isset($_REQUEST['update_price_flag']) ) {
        $cart_item_data['custom_data']['quantity'] = $_REQUEST['quantity_prod'];
        $cart_item_data['custom_data']['product_id'] = $product_id;
        $data['quantity'] = $_REQUEST['quantity_prod'];
    }

    if(isset($_SESSION['frontvector_'.$product_id]) && isset($_REQUEST['update_price_flag']))
    {
        $cart_item_data['custom_data']['frontvector'] = $_SESSION['frontvector_'.$product_id];
        $cart_item_data['custom_data']['frontsvg'] = $_SESSION['frontsvg_'.$product_id];
        $cart_item_data['custom_data']['backsvg'] = $_SESSION['backsvg_'.$product_id];
        $data['frontvector'] = $_SESSION['frontvector_'.$product_id];
        $data['frontsvg'] = $_SESSION['frontsvg_'.$product_id];
        $data['backsvg'] = $_SESSION['backsvg_'.$product_id];

    }
    if(isset($_SESSION['pdftvector_'.$product_id]) && isset($_REQUEST['update_price_flag']))
    {
        $cart_item_data['custom_data']['pdftvector'] = $_SESSION['pdftvector_'.$product_id];
        $data['pdftvector'] = $_SESSION['pdftvector_'.$product_id];
    }

    if( isset($_REQUEST['update_price_flag']))
    {
        // below statement make sure every add to cart action as unique line item
        $cart_item_data['custom_data']['unique_key'] = md5( microtime().rand() );
        WC()->session->set( 'price_calculation', $data );

        $_SESSION['wdm_user_custom_data'] = $cart_item_data['custom_data'];
    }

    return $cart_item_data;
}

//Step 3: you need to override the item price with your custom calculation. It will work with your every scenario of your single product sessions.

add_action( 'woocommerce_before_calculate_totals', 'add_custom_item_price', 10 );
function add_custom_item_price( $cart_object ) {

    foreach ( $cart_object->get_cart() as $item_values ) {
        // echo "<pre>"; 
        // print_r($item_values);
        // exit;
        if (isset($item_values['custom_data']['product_id']) && $item_values['custom_data']['product_id'] == $item_values['product_id']  )
        {
            ##  Get cart item data
            $item_id = $item_values['data']->id; // Product ID
            $original_price = $item_values['data']->price; // Product original price

            ## Get your custom fields values
            $price1 = $item_values['custom_data']['price_prod'];
            $quantity = $item_values['custom_data']['quantity_prod'];

            // CALCULATION FOR EACH ITEM:
            ## Make HERE your own calculation 
            $new_price = (int)$price1 + $original_price;
            //$new_price = 10 ;
            
            ## Set the new item price in cart
            $item_values['data']->set_price($new_price);
        }
    }
}


add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session',1,3);
function wdm_get_cart_items_from_session($item,$values,$key) {
    if (array_key_exists( 'custom_data', $values ) ) {
        $item['custom_data'] = $values['custom_data'];
    }
    return $item;
}

add_filter('woocommerce_cart_item_name','add_usr_custom_session',1,3);
function add_usr_custom_session($product_name, $values, $cart_item_key ) {
    session_start();

    if(isset( $values['custom_data']['product_id']))
    {
        //print_r($_SESSION);
        $prd_id = $values['custom_data']['product_id'];

        //$return_string = $product_name . "<br />" . $values['custom_data']['quantity'] . "<br />" . $values['custom_data']['product_id'];
        $return_string = '';
        if(isset($_SESSION['frontvector_'.$prd_id]))
        {
            $price = $_SESSION['one_side_price_cart_'.$prd_id];
           // $price = (float)$price.'.00';
            $price = wc_price( $price, array() );
            //$html = $product_name . "<br/> Knife Design: <img src='".$_SESSION['frontvector_'.$prd_id]."' width='50' height='50' /><br/><p> One side Price:   ".($price)." </p>";
            $html = $product_name . "<br/><span> One side Price:".($price)." </span>";

            $return_string .= $html;
        }

        if(isset($_SESSION['backendvector'.$prd_id]))
        {
            $price = $_SESSION['sec_side_price_cart_'.$prd_id];
            $price =  wc_price( $price, array() );
            //$html ="<br /><img src='".$_SESSION['backendvector'.$prd_id]."'  width='50' height='50'  /> <span> Second Side Price:  ". ($price)."</span>" ;            
            $html ="<br /><span> Second Side Price:  ". ($price)."</span>" ;            
            $return_string .= $html;
        }

        if(isset($_SESSION['frontvector_'.$prd_id]) || isset($_SESSION['backendvector'.$prd_id]) )
        {
            $product = wc_get_product( $prd_id );
            $html ="<br /><span> Product Price:  ". ($product->get_price())."</span>" ;
            $return_string .= $html;
        }

       

        if(isset($_SESSION['pdftvector_'.$prd_id]))
        {
            $html ="<br /><a href=".$_SESSION['pdftvector_'.$prd_id].">View Knife Designed</a>";
            $return_string .= $html;
        }
        
        return $return_string;
    }
}

add_action('woocommerce_add_order_item_meta','wdm_add_values_to_order_item_meta',1,2);
function wdm_add_values_to_order_item_meta($item_id,$values) {
    
    //print_r($item_id);

    global $woocommerce,$wpdb;

    //wc_add_order_item_meta($item_id,'One Side File','<a href="'.$values['custom_data']['frontvector'].'">View </a>');
    //wc_add_order_item_meta($item_id,'customer_image',$values['_custom_options']['another_example_field']);
    if(!empty($values['custom_data']['frontsvg'])) {
        wc_add_order_item_meta($item_id,'FRONT SVG','<a href="'.$values['custom_data']['frontsvg'].'" target="_blank">View FRONT Designed Knife</a>');
    }
   
    if(!empty($values['custom_data']['backsvg'])) {
       wc_add_order_item_meta($item_id,'BACK SVG','<a href="'.$values['custom_data']['backsvg'].'" target="_blank">View BACK Designed Knife</a>'); 
    }
    // wc_add_order_item_meta($item_id,'PDF','<a href="'.$values['custom_data']['pdftvector'].'">View Designed Knife</a>');
    
    //wc_add_order_item_meta($item_id,'_hidden_field',$values['custom_data']['product_id']);
}

add_action('wp_enqueue_scripts','home_page_head_foot');
function home_page_head_foot()
{
    if(is_front_page() || is_page(4266))
    {
        echo '<style>#footer{display:none}</style>';
    }
}

/*add configurator link on product single page based on set in admin side*/
add_action( 'woocommerce_single_product_summary', 'custom_text', 15 );
function custom_text() {

    global $product;

    $configuratorLink = get_post_meta( $product->get_id(),'v3d_knife_configurator_enable', true);
    if(!$configuratorLink)
    {
        echo "<a href='".site_url('konfigurator?prod_id='.$product->get_id().'&prod_name='.$product->get_name())."'>RUN CONFIGURATOR</a>";
    }
}

add_filter('woocommerce_product_data_tabs', 'v3d_knife_settings_tabs' );
function v3d_knife_settings_tabs( $tabs ) {
	//unset( $tabs['inventory'] );
	$tabs['v3d_knife'] = array(
		'label'    => 'Knife Settings',
		'target'   => 'v3d_knife_data',
		//'class'    => array('show_if_virtual'),
		'priority' => 21,
	);
	return $tabs;
}
 
/*
 * Tab content
 */
add_action( 'woocommerce_product_data_panels', 'v3d_knife_settings' );
function v3d_knife_settings() {

	echo '<div id="v3d_knife_data" class="panel woocommerce_options_panel hidden">';
	woocommerce_wp_text_input( 
        [
            'id'          => 'v3d_knife_one_side_price',
            'value'       => get_post_meta( get_the_ID(), 'v3d_knife_one_side_price', true ),
            'label'       => 'one side price',
            'desc_tip'    => true,
            'description' => 'Define one side price'
        ]
    );
    woocommerce_wp_text_input( 
        [
            'id'          => 'v3d_knife_second_side_price',
            'value'       => get_post_meta( get_the_ID(), 'v3d_knife_second_side_price', true ),
            'label'       => 'second side price',
            'desc_tip'    => true,
            'description' => 'Define the second side price'
        ]
    );
    woocommerce_wp_checkbox(
        [		
            'id'          => 'v3d_knife_configurator_enable',
            'value'       => get_post_meta( get_the_ID(), 'v3d_knife_configurator_enable', true ),
            'label'       => 'Configurator Disable',
            'desc_tip'    => true,
            'description' => 'Disable configurator for this product',
        ]
    );
	echo '</div>';
}

add_action( 'woocommerce_process_product_meta', 'misha_save_fields', 10, 2 );
function misha_save_fields( $id, $post ) {

    if( !empty( $_POST['v3d_knife_configurator_enable'] ) )
    {
		update_post_meta( $id, 'v3d_knife_configurator_enable', $_POST['v3d_knife_configurator_enable'] );
    }
    else
    {
		delete_post_meta( $id, 'v3d_knife_configurator_enable' );
    }
    
    if( !empty( $_POST['v3d_knife_one_side_price'] ) )
    {
		update_post_meta( $id, 'v3d_knife_one_side_price', $_POST['v3d_knife_one_side_price'] );
    }
    else
    {
		delete_post_meta( $id, 'v3d_knife_configurator_enable' );
    }

    if( !empty( $_POST['v3d_knife_second_side_price'] ) )
    {
		update_post_meta( $id, 'v3d_knife_second_side_price', $_POST['v3d_knife_second_side_price'] );
    }
    else
    {
		delete_post_meta( $id, 'v3d_knife_second_side_price' );
	}
}

add_action('wp_head','get_hme_url',1);
function get_hme_url()
{
    ?>
    <script>var home_page_ur = "<?php echo site_url(); ?>"</script>
<?php
}


add_action("init",'off_speed_optimize_configurator');
function off_speed_optimize_configurator()
{
    $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    if(strpos($current_url,'/konfigurator') !== false && !isset($_GET['ModPagespeed']))
    {
        $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $redirect = add_query_arg( array(
            'ModPagespeed' => "off",
            //'key2' => 'value2',
        ), $current_url );
        exit(wp_redirect($redirect));
    }    
}

//set knife price in general settings
add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section', // Section ID 
        'Knife Price Setting', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'one_side_price', // Option ID
        'One Side Price', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'one_side_price' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'second_side_price', // Option ID
        'Second Side Price', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'second_side_price' // Should match Option ID
        )  
    ); 

    register_setting('general','one_side_price', 'esc_attr');
    register_setting('general','second_side_price', 'esc_attr');
}

function my_section_options_callback()
{
    echo '<p>A little message on editing info</p>';  
}

function my_textbox_callback($args)
{
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}



//get configurator and componenets fro frot-end side
add_action('wp_ajax_template_gallery', 'template_gallery');
add_action('wp_ajax_nopriv_template_gallery', 'template_gallery');
function template_gallery()
{

    if(isset($_POST['modula_id']) && !empty($_POST['modula_id']))
    {
        $rr = do_shortcode('[modula id="'.$_POST['modula_id'].'"]');
        echo json_encode(array('data'=>$rr));
        exit;
    }

    $args = array(  
        'post_type' => 'modula-gallery',
        'post_status' => 'publish' 
    );
    $loop = new WP_Query( $args ); 
    if(isset($loop->posts) && !empty($loop->posts))
    {
        $hmtl = '';
        foreach($loop->posts as $post)
        {
            $hmtl .= '<div class="gallery-category-block">';
            $hmtl .='<span><a href="javascript:void(0)" class="cat-gallery-d" modula-id="'.$post->ID.'">'.$post->post_title.'</a></span>';
            $hmtl .= '</div>';
        }
        echo json_encode(array('data'=>$hmtl));
    }
    exit();
}


add_action('wp_head','get_3d_file');
function get_3d_file()
{
    echo '<script>var model3d_file_url = "";</script>';
    if(isset($_GET['prod_id']))
    {
        $get_3d_file = get_post_meta($_GET['prod_id'],'3d-modell',true);
        if(!empty($get_3d_file))
        {
            $attachment_url = wp_get_attachment_url( $get_3d_file);
            echo '<script>var model3d_file_url = "'.$attachment_url.'"</script>';
        }
    }
}

add_action('wp_head','register_g_fonts');
function register_g_fonts()
{
    echo '<link rel="preconnect" href="https://fonts.gstatic.com">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Aladin&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Bevan&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Charm&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Aclonica&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Alegreya+Sans+SC:wght@300&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Pattaya&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Asul:wght@400;700&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Black+And+White+Picture&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=PT+Serif&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Castoro&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Crafty+Girls&display=swap" rel="stylesheet">';
    echo '<link href="https://fonts.googleapis.com/css2?family=Princess+Sofia&display=swap" rel="stylesheet">';
}
function svg_format($data) {
    $svg = '';
    $svg .= '<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg version="1.1" width="1024" height="512" viewBox="0 0 1024 512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs></defs><g></g>';
    $svg .= '<g transform="">';
    $svg .= '<image x="0" y="0" width="1024" height="512" preserveAspectRatio="none" xlink:href="'.$data.'"></image>';
    $svg .= '</g>';
    $svg .= '</svg>';
    return $svg;
}