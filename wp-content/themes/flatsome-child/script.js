jQuery(document).ready(function ($) {
    "use strict";

    jQuery('body').on('click',function() {
        if(jQuery(this).parents().hasClass('classname')) {
            setTimeout(function(){
                alert(jQuery('iframe').attr(src));
            }, 3000);
        }
    });

    jQuery('body').on('click','.load-gallery-d , .cat-gallery-d',function() {

        let flag_back = 0;
        if($(this).hasClass('cat-gallery-d')) {
            var data = {
                'action' : 'template_gallery',
                'modula_id' :$(this).attr('modula-id')
            };
            flag_back = 1;
        } else {
            var data = {
                'action': 'template_gallery',
            };
            flag_back = 0;
        }
       
        jQuery.post(flatsomeVars.ajaxurl, data, function(response) {

            let res = JSON.parse(response);
            jQuery('#mainmenucontainer').html(res.data);
            $("#mainmenucontainer").prepend('<span><i class="fa fa-close close-text-d gallery-custom-cross"></i></span><span class="gallery-back-s"><a href="javascript:void(0)" class="load-gallery-d ">Back</a></span>');

            jQuery('#mainmenucontainer').show();

            if(flag_back == 1) {
                $('.gallery-back-s').show();
            } else {
                $('.gallery-back-s').hide();
            }
        });
    });
    
    jQuery('body').on('click','.gallery-custom-cross',function() {
        jQuery('#mainmenucontainer').html('');
        jQuery('#mainmenucontainer').hide();
    });
    jQuery('body').on('click',function() {
        
        if(jQuery('#svgfrontside').length > 0 && ( jQuery('#svgfrontside').html().length > 0 ||  jQuery('#svgbackside').html().length > 0) )
        {
            jQuery('.load-front-img').attr("href",home_page_ur+"/wp-content/uploads/tmkconfigurator/pdf/"+v3d_GUID+".pdf");
            // if(jQuery('#svgfrontside > div').length > 0)
            // {
            //     jQuery('.load-front-img').attr("href",home_page_ur+"/wp-content/uploads/tmkconfigurator/"+v3d_GUID+"one.jpeg");
            // }
            // if(jQuery('#svgbackside > div').length > 0)
            // {
            //     jQuery('.load-back-img').attr("href",home_page_ur+"/wp-content/uploads/tmkconfigurator/"+v3d_GUID+"two.jpeg");
            // }
            // jQuery('.view-designed-knife, .view-designed-knife-ul').show();

        }
        else
        {
            jQuery('.view-designed-knife, .view-designed-knife-ul').hide();
        }
    });


    jQuery(".upload-pic-s , .upload-pic-ul-s").hover(function() {

        jQuery('.upload-pic-ul-s').removeClass('hide-pic-ul');
      }, function() {
        jQuery('.upload-pic-ul-s').addClass('hide-pic-ul');
      });
      
      jQuery(".view-designed-knife , .view-designed-knife-ul-s").hover(function() {

        jQuery('.view-designed-knife-ul-s').removeClass('view-designed-knife-ul');
      }, function() {
        jQuery('.view-designed-knife-ul-s').addClass('view-designed-knife-ul');
      });



});
