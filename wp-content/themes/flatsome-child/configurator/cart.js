
jQuery(document).ready(function ($) {
    "use strict";

    jQuery('body').on('click', '.close-text-d', function () {

        jQuery('#v3d_text').val('');
        jQuery('.text-block-d').hide();
    });

    jQuery('body').on('click', '.close-mnaufacturer-d', function () {
        jQuery('#mainmenucontainer').hide();
    });

    var urlParams = new URLSearchParams(window.location.search);
    var prod_id_init = '';
    if(urlParams.has('prod_id') == true) {
        prod_id_init = urlParams.get('prod_id');
    } else {
        prod_id_init= 1330;
    }

    //get knife price
    var data = {
        'action': 'get_knife_price',
        'product_id': prod_id_init,
    };

    jQuery.post(ve3_product_ajax.ajaxurl, data, function (response) {
        let res = JSON.parse(response);
        one_side_price = res.one_side;
        sec_side_price = res.sec_side;
    });

    /* Write your custom JS below */
    jQuery('body').on('click', '.add-cart-v3d-d', function (e) {
        jQuery('.load-d').show();
        let one_side_price_cart,sec_side_price_cart=0; 
        if(firstside_price ) {
            one_side_price_cart = parseInt(one_side_price);
        }
        if(secside_price ) {
            sec_side_price_cart = parseInt(sec_side_price);
        }

        let v3d_selected_product = jQuery('#v3d_selected_product').val(v3d_product_id);
        var data = {
            'action': 'send_subscribe_email',
            'product_id': v3d_selected_product.val(),
            'price_prod': knife_price_calc,
            'quantity_prod': 1,
            'update_price_flag':1,
            'one_side_price_cart':one_side_price_cart,
            'sec_side_price_cart':sec_side_price_cart
        };
        jQuery.post(ve3_product_ajax.ajaxurl, data, function (response) {
            jQuery('.load-d').hide();
            window.location.href = home_page_ur+'/cart';
        });
    });
});