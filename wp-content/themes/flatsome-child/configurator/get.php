<?php
    session_start();

    require_once("../../../../wp-load.php");

    $_SESSION['previous_site'] = str_replace("page=page=","page=",$_SESSION['current_site']);
    $_SESSION['current_site'] = str_replace("page=page=","page=",$_SERVER['QUERY_STRING']);;
   
    
    if(isset($_GET))
    {
        function wc_subcats_from_ID($product_cat_ID) {
            $args = array(
                'hierarchical' => 1,
                'show_option_none' => '',
                'hide_empty' => 1,
                'parent' => $product_cat_ID,
                'taxonomy' => 'product_cat'
            );
            $subcats = get_categories($args);
            if(!empty($subcats))
            {
                return($subcats);
            }
        }

        function wc_subcats_from($parent_cat_NAME) {

            $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
            $product_cat_ID = $IDbyNAME->term_id;

            $args = array(
                'hierarchical' => 1,
                'show_option_none' => '',
                'hide_empty' => 1,
                'parent' => $product_cat_ID,
                'taxonomy' => 'product_cat'
            );
            $subcats = get_categories($args);
            if(!empty($subcats))
            {
                return($subcats);
            }
        }

        function GetProductAllParamsById($_idProduct = null) {

            if ( func_num_args() > 0 )
            {
                $result = Array();
                $productAllAttr = get_post_meta( $_idProduct, '_product_attributes' );
                foreach ($productAllAttr as $value) 
                {
                    while (count($value) > 0) 
                    {
                        $_instValue = array_pop($value);
                        $_nameParam = $_instValue['name'];
                        $_nameProductAttr = wc_get_product_terms( $_idProduct, $_nameParam, array( 'fields' => 'names' ) );
                        array_push($result, [$_nameParam => $_nameProductAttr]);
                        //array_push($result, $_nameParam, $_nameProductAttr);
                    }
                }
            }
            return isset($result) ? $result : null;
        }


        $call = array_key_first($_GET);

        if ($call == 'catlist')
        {
            $product_args = array(
                'status' => 'publish',
                'limit' => -1,
                'category' => $_GET['catlist']
            );
            $products = wc_get_products($product_args);

            $content = '<div id="messer-list" style="padding-top:110px;">';
            foreach ( $products as $product )
            {
                $image = wp_get_attachment_url( $product->get_image_id() );
                $name = $product->get_title();
                $preis = $product->get_price();
                $link3d = get_field('3d-modell', $product->get_id());
                $id = $product->get_id();

                $content.='<div class="messer-tile" onclick="Knife.switch(\'' . $id . '\')">';

                //$content.='<div class="messer-tile" onclick="alert(\'3d / einzelansicht hier\')">';
                //$thumbnail_id = get_woocommerce_term_meta( $product->term_id, 'thumbnail_id', true );
                $content.='<div class="messer-img-container"><img class="messer-img" src="' . $image . '"></div>';
                $content.='<div class="messer-img-label">' . $name . '<br/><b>' . $preis . ',- €</b></div>';
                $content.='</div>';
            }
            $content .= '</div>';
        }
        else if ($call == 'knife')
        {
            //$knife = wc_get_product($_GET['knife']);
            //$content = $knife;
            $product = wc_get_product($_GET['knife']);
            $link3d = get_field('3d-modell', $product->get_id());

            //$attributes = wc_get_attributes($product);
            $attributes = GetProductAllParamsById($_GET['knife']);

            /*
            $attributes = ( object ) array (
            'color' => $product->get_attribute( 'pa_color' ),
            'size' => $product->get_attribute( 'pa_size' ),
            );*/

            $data = array(
            'id' => $_GET['knife'],
            'name' => $product->get_title(),
            'preis' => $product->get_price(),
            'beschreibung' => $product->get_description(),
            'eigenschaften' => $attributes,
            'link3d' => $link3d
            );
            $content = json_encode($data);
        }
        else if ($call == 'page') 
        {
            $pagename = $_GET['page'];

            if ($pagename == 'hersteller')
            {
                $herstellertiles = '<div class="hersteller-grid">';
                $hersteller = wc_subcats_from('Hersteller');
                foreach($hersteller as $marke)
                {
                    $serien = wc_subcats_from($marke->name);
                    $url = get_category_link($marke);
                    $herstellertiles.='<div class="prod-tile" onclick="mainMenu.loadSite(\'marke&marke=' . $marke->term_id . '\')">';

                    $thumbnail_id = get_woocommerce_term_meta( $marke->term_id, 'thumbnail_id', true );
                    $image = wp_get_attachment_url( $thumbnail_id );

                    //$herstellertiles.='<div class="prod-tile" onclick="p5s.imageLayer(\''.$image.'\')">';
                    $thumbnail_id = get_woocommerce_term_meta( $marke->term_id, 'thumbnail_id', true );
                    $image = wp_get_attachment_url( $thumbnail_id );
                    $herstellertiles.='<div class="prod-img-container"><img class="thumb-hersteller" src="' . $image . '"></div>';
                    $herstellertiles.='</div>';
                }
                $herstellertiles .= '</div>';
                preg_replace( "/\r|\n/", "", $herstellertiles );
                $headline = 'Hersteller';
                $navi = '<ul><li><a href="#" onclick="mainMenu.loadSite(\'types\')">Typen</a></li><li><a href="#" onclick="mainMenu.loadSite(\'3d\')">Produkte mit 3D-Modell</a></li></ul>';
                $content = $herstellertiles;
            }
            else if ($pagename == 'marke')
            {
                if (isset($_GET['marke']))
                {
                    $marke =  get_the_category_by_ID($_GET['marke']);
                    $serien = wc_subcats_from_ID($_GET['marke']);
                    $serientiles = '<div id="serien-list">';
                    foreach($serien as $serie)
                    {
                        $serientiles.='<div class="serien-tile" onclick="mainMenu.loadSite(\'serie&serie=' . $serie->term_id . '\')">';
                        $thumbnail_id = get_woocommerce_term_meta( $serie->term_id, 'thumbnail_id', true );
                        $image = wp_get_attachment_url( $thumbnail_id );
                        $serientiles.='<div class="serie-img-container"><img class="serie-img" src="' . $image . '"></div>';
                        $serientiles.='<div class="serie-img-label">' . $serie->name . '</div>';
                        $serientiles.='</div>';
                    }
                    $headline = $marke;
                    $navi = '<a href="#" onclick="mainMenu.loadSite(\'hersteller\')">Zurück</a>';
                    $content = $serientiles;
                    $serientiles .= '</div>';
                }
            }
            else if ($pagename == 'serie')
            {
                //echo('<script>alert("' . $_GET['sid'] . '");</script>');
                $serie = get_the_category_by_ID($_GET['serie']);
                $product_args = array(
                    'status' => 'publish',
                    'limit' => -1,
                    'category' => $serie
                );
                $products = wc_get_products($product_args);
                $messertiles = '<div id="messer-list">';
                foreach($products as $product)
                {

                    $id = $product->get_id();
                    $messertiles.='<div class="messer-tile" onclick="Knife.switch(\'' . $id . '\')">';

                    //$messertiles.='<div class="messer-tile" onclick="alert(\'3d / einzelansicht hier\')">';
                    //$thumbnail_id = get_woocommerce_term_meta( $product->term_id, 'thumbnail_id', true );
                    $image = wp_get_attachment_url( $product->get_image_id() );
                    $messertiles.='<div class="messer-img-container"><img class="messer-img" src="' . $image . '"></div>';
                    $messertiles.='<div class="messer-img-label">' . $product->name . '<br/><b>' . $product->price . ',- €</b></div>';
                    $messertiles.='</div>';
                }
                $messertiles .= '</div>';

                $headline = $serie;

                $back = str_replace("page=page=","page=",$_SESSION['previous_site']);
                $navi = '<a href="#" onclick="mainMenu.loadSite(\'' . $back . '\');">Zurück</a>';
                $content = $messertiles;
            }
            else if ($pagename == 'types')
            {
                $back = str_replace("page=page=","page=",$_SESSION['previous_site']);
                //$headline = $serie;
                $headline = "Typen";
                $navi = '<a href="#" onclick="mainMenu.loadSite(\'' . $back . '\');">Zurück</a>';
                $content = '<div id="verwendungen-list"></div>';
                $content .= '<script>';
                $content .= 'jQuery(\'.verwendungen-slider\').prependTo(\'#main-menu-content\');';
                $content .= 'jQuery(".verwendungen-slider").fadeIn("fast");';
                $content .= 'var activeUse =  jQuery(\'.slider\').find(\'div[aria-selected="true"]\').find(\'.caption\').text();';
                $content .= "loadList('category', activeUse);";
                $content .= '</script>';
            } 
            else if ($pagename == '3d')
            {
                $product_args = array(
                    'status' => 'publish',
                    'limit' => -1,
                    'category' => 'Verwendung'
                );
                $products = wc_get_products($product_args);

                $messertiles = '<div id="messer-list">';
                foreach ( $products as $product )
                {
                    $link3d = get_field('3d-modell', $product->get_id());
                    if($link3d != '')
                    {
                        $image = wp_get_attachment_url( $product->get_image_id() );
                        $name = $product->get_title();
                        $preis = $product->get_price();
                        $id = $product->get_id();
                        $messertiles.='<div class="messer-tile" onclick="Knife.switch(\'' . $id . '\')">';
                        $messertiles.='<div class="messer-img-container"><img class="messer-img" src="' . $image . '"></div>';
                        $messertiles.='<div class="messer-img-label">' . $name . '<br/><b>' . $preis . ',- €</b></div>';
                        $messertiles.='</div>';
                    }
                }
                $messertiles .= '</div>';
                $back = str_replace("page=page=","page=",$_SESSION['previous_site']);
                $headline = "Produkte in 3D";
                $navi = '<a href="#" onclick="mainMenu.loadSite(\'' . $back . '\');">Zurück</a>';
                $content = $messertiles;

            }
            else
            {
                exit("EXIT");
            }

            $page = '';
            $page .= '    <div id="main-menu-frame">';
            $page .= '<span><i class="fa fa-close close-mnaufacturer-d"></i></span>';
            $page .= '         <div id="main-menu-left">';
            $page .= '           <h2 id="main-menu-headline">';
            $page .= $headline;
            $page .= '           </h2>';
            $page .= '           <div id="main-menu-navi">';
            $page .= $navi;
            $page .= '           </div>';
            $page .= '         </div>';
            $page .= '         <div id="main-menu-center">';
            $page .= '           <div id="main-menu-content">';
            $page .= $content;
            $page .= '           </div>';
            $page .= '         </div>';
            $page .= '       </div>';

            $content = $page;
        }
        //$page = $content;
        //echo($page);
        echo($content);
    }