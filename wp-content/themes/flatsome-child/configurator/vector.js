
//P5

//var canvas;
let renderSvgGraphics;
//let textRenderGraphics;

//confiUrl = knifeUrl;
let svgmcx = 0;
let svgmxy = 0;
let svglayer = [];
//let textlayer = [];
let svgrotateicon, svgscaleicon, svgtrashicon;
//let currentSide = true;
//let scene1 = true;

let renderSVG;



let svgketch = function(p) {
  var img, svgimageBrowse, imageBtn, textBtn, getVecBtn;
  let UVhull = [];
  let UVcoords = [];
  let UVmedian;


  p.preload = function(){
    svgrotateicon = p.loadImage('../wp-content/themes/flatsome-child/configurator/img/rotate40x40.png');
    svgtrashicon = p.loadImage('../wp-content/themes/flatsome-child/configurator/img/trash40x40.png');
    svgscaleicon = p.loadImage('../wp-content/themes/flatsome-child/configurator/img/scale40x40.png');
  }
  p.setup = function(){
    
    renderSvgGraphics = p.createGraphics(1024,512 );
    renderSvgGraphics.imageMode(p.CENTER);
    renderSvgGraphics.blendMode(p.MULTIPLY);

    svgimageBrowse = p.createFileInput(function(file){
      p.loadImage(file.data, img => {
       
        svglayer.push(new SvgLayer(img,svglayer.length,1));
        for(var i = svglayer.length-1; i > -1; i--){
          svglayer[i].deselect();
        }
        svglayer[svglayer.length-1].select();
        p.redraw();
      });
    },function(){
      //svglayer[svglayer.length-1].select();
    });
    svgimageBrowse.id("svgimageBrowse");
    //imageBtn.position(0,210);
    svgimageBrowse.hide();

/*
//jQuery('#imageButton').css("left","0").value("yo");
textBtn = p.createButton('Text hinzufügen');
textBtn.position(420, 50);
textBtn.mousePressed(p.addText);
switchSceneBtn = p.createButton('Messer wechseln');
switchSceneBtn.position(420, 80);
switchSceneBtn.mousePressed(p.switchScene);
priceBtn = p.createButton('Preis berechnen');
priceBtn.position(420, 110);
priceBtn.mousePressed(p.calcPrice);
*/

renderSVG = p.createCanvas(100, 100, p.SVG);
renderSVG.id('rendersvg');


jQuery('#rendersvg').css("width","400px").css("height","200px"); //calc(50% - 400px/2)

//p.save('S.svg');
//p.print("saved svg");
console.log(confiUrl);
console.log("confiUrl......");
init3D(confiUrl);
//switchScene(confiUrl);

p.imageMode(p.CENTER);

}

p.switchSide = function(){
  for(var i = 0; i < svglayer.length; i++){
    // console.log(svglayer[i].side);
  }
  currentSide = !currentSide;

  var mask = getMask();
  p.getHull(mask.geometry.attributes.uv.array);

  for(var i = 0; i < svglayer.length; i++){
    if(svglayer[i].side == currentSide){
      for(var j = i-1; j > -1; j--){
        svglayer[j].deselect();
      }
      svglayer[i].select();
    } else {
      svglayer[i].deselect();
    }
  }
  // console.log(currentSide);
}


var textBox = '';
var canvasss = '';
p.openTextBlock = function ()
{
  jQuery('.text-block-d').show();

  canvasss = new fabric.Canvas('fabrics_canvas_append');
  textBox=new fabric.Textbox("Enter Text Heres",{
    fontSize: 30,
    fontFamily: 'sans-serif',
    //textAlign: 'left',  
    width: 200, // for 20 characters
    fill:'#fff',
    //top:arrowTop,
    left:'50%',
    textAlign: 'center',  //"left", "center", "right" or "justify".
    });
    canvasss.add(textBox);

    canvasss.renderAll();
    console.log(canvasss.toSVG());
}

p.addText = function(){
  var align = ["left", "center", "right" , "justify"];
  var val = align[Math.floor(Math.random() * align.length)];
 
  textBox.set({ width: 100, height: 50 , fontSize: 10,top:10,left:20,'textAlign':val});
  canvasss.renderAll();
  
  jQuery('.text-block-d').hide();
  jQuery('#rendersvg').css("width","400px").css("height","200px");
  jQuery('#rendersvg').css("width","400px").css("height","200px");
  jQuery('#svgcontainer').show();
  // p.imagesvglayer(img);
//p.redraw();
}

p.imageLayer = function (imageData,hideELement = '') {

  console.log(imageData);
  p.loadImage(imageData, img => {

    svglayer.push(new SvgLayer(img,svglayer.length,1));
    for(var i = svglayer.length-1; i > -1; i--){
      svglayer[i].deselect();
    }
    svglayer[svglayer.length-1].select();
    p.redraw();
  });
  
  if(hideELement)
  {
    $(hideELement).hide();
  }

}




p.saveSVG = function(){

  p.save("mySVG.svg");
      p.noLoop();
//   p.redraw();
//  // p.saveJSON('beatles')
//   p5render.saveScreenshot();
}

//  p.switchScene = function(){
//  switchKnife(knifeUrl);
/*
let url;
if(scene1){
url = 'https://www.typemyknife.de/test5.glb.xz';
} else {
url = 'https://www.typemyknife.de/wip2/wp-content/uploads/2020/02/DICK-1778-Chinesisches-Kochmesser.glb.xz';
}
var path = url.replace(/^.*\/\/[^\/]+/, '');
switchScene(path);
scene1 = !scene1;
*/
//}


p.draw = function(){
  //p.background(127);
  //renderSvgGraphics.background(255);
  console.log("Create Canvas .......");
  //p.clear();
  //jQuery("#rendersvg")[0].getContext('2d').clearRect(0,0,p.width,p.height); //SLOW AS FUCK! WHY DOES CLEAR NO LONGER WORK?

  if(UVhull != ''){
    renderSvgGraphics.clear();
    renderSvgGraphics.beginShape();
    renderSvgGraphics.noStroke();
    renderSvgGraphics.fill(255);
    for (let point of UVhull) {
      renderSvgGraphics.vertex(point.x, point.y);
    }
    renderSvgGraphics.endShape(p.CLOSE);

    for(var i = 0; i < svglayer.length; i++){
      if(svglayer[i].side == currentSide){
        svglayer[i].renderGfx();
      }
    }

  }


  p5render.graphics(renderSvgGraphics);
  updateMask();
  app.render();

  p.image(renderSvgGraphics,p.width/2,p.height/2);
  p.fill(5,10,0,50);
  p.rect(0,0,p.width,p.height);
	// console.log(renderSvgGraphics);

  if(UVhull != ''){
    p.beginShape();
    p.noStroke();
    p.fill(0,0,0,80);
    p.vertex(0,0);
    p.vertex(0,p.height);
    p.vertex(p.width,p.height);
    p.vertex(p.width,0);
    p.beginContour();
    for (let point of UVhull) {
      p.vertex(point.x, point.y);
    }
    p.endContour();
    p.endShape(p.CLOSE);


    p5render.UVcutout(UVhull);
  }


  for(var i = 0; i < svglayer.length; i++){
    if(svglayer[i].side == currentSide){
      svglayer[i].renderOverlays();
    }
  }

  p.noLoop();
}




p.mouseDragged = function(){
  if (p.pressedInside && p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0){

    console.log("mouseDragged....Rotate.....DRAG......SCALE.......")

    for(var i = 0; i < svglayer.length; i++){
      if(svglayer[i].selected){
        
        console.log("svglayer[i].scaling");
        if(svglayer[i].scaling){
          svglayer[i].scale();
        } else if (svglayer[i].rotating){
          svglayer[i].rotate();
        } else {
          svglayer[i].drag();
        }
        p.redraw();
        return;
      }
    }
  }
}
p.mouseReleased = function(){
  console.log("Job Done");
  for (var i = 0; i < svglayer.length; i++){
    svglayer[i].rotating = false;
    svglayer[i].dragging = false;
    svglayer[i].scaling = false;
  }
  //if(cameraSide != currentSide){
  //  p.switchSide();
  //}

  p.pressedInside = false;
  p.redraw();
}
var pressedInside = false;
p.mousePressed = function(){
    console.log('X:'+p.mouseX);
    console.log('Y:'+p.mouseY);
  if (p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0 && jQuery('#svgcontainer').is(':visible')){
    p.pressedInside = true;
    svgmcx = p.mouseX;
    mcy = p.mouseY;
    /*
    for(var i = svglayer.length-1; i > -1; i--){
    if(svglayer[i].hover() == true && svglayer[i].selected == false){
    svglayer[i].select();
    //svglayer.push(svglayer.splice(i, 1)[0]);
    return;
  }*/

  if(svglayer[svglayer.length-1] != undefined && svglayer[svglayer.length-1].selected){
    // console.log("have a selected svglayer");
  } else {
    //console.log("no selected svglayer");
  }


  if(p.mouseX > svgScalex-20 && p.mouseX < svgScalex+20 && p.mouseY > svgScaley-20 && p.mouseY < svgScaley+20){
    svglayer[svgSelectedLayer].scaling = true;
  }
  if(p.mouseX > svgRotatex-20 && p.mouseX < svgRotatex+20 && p.mouseY > svgRotatey-20 && p.mouseY < svgRotatey+20){
    svglayer[svgSelectedLayer].rotating = true;
  }
  if(p.mouseX > svgTrashx-20 && p.mouseX < svgTrashx+20 && p.mouseY > svgTrashy-20 && p.mouseY < svgTrashy+20){
    svglayer[svgSelectedLayer].trash();
    for(var i = 0; i < svglayer.length; i++){
      svglayer[i].layerNumber = i;
    }
  }

  if(svglayer[svgSelectedLayer] != undefined){
    if(!svglayer[svgSelectedLayer].scaling && !svglayer[svgSelectedLayer].rotating){
      for(var i = svglayer.length-1; i > -1; i--){
        svglayer[i].deselect();
        if(svglayer[i].hover()) {
          for(var j = i-1; j > -1; j--){
            svglayer[j].deselect();
          }
          svglayer.push(svglayer.splice(i, 1)[0]);
          svglayer[svglayer.length-1].select();
          return;
        }
      }
    }
  }
  p.redraw();
}
}
p.mouseClicked = function(){

}



p.getHull = function(arr){
  var points = [];
  for (let i = 0; i < arr.length; i+=2) {
    points.push(p.createVector(p.map(arr[i],0,1,0,p.width),p.map(arr[i+1],0,1,0,p.height)));
  }
  points.sort((a, b) => a.x - b.x);
  leftOf = function(v1, v2, p) {
    const cross = p5.Vector.sub(v2, v1).cross(p5.Vector.sub(p, v1));
    return cross.z < 0;
  }
  let leftMost = points[0];
  let currentVertex = leftMost;
  UVhull = [];
  UVhull.push(currentVertex);
  let nextVertex = points[1];
  let index = 2;
  let nextIndex = -1;
  UVmedian = p.createVector(0,0);
  while (true){
    let checking = points[index];
    if (checking == undefined){
      if(UVhull != []){
        for(var i = 0; i < UVhull.length; i++){
          UVmedian = p5.Vector.add(UVmedian,UVhull[i]);
        }
        UVmedian.x = UVmedian.x / UVhull.length;
        UVmedian.y = UVmedian.y / UVhull.length;
        let UVx = 0;
        let UVy = 0;
        let UVw = 0;
        let UVh = 0;
        for (let point of UVhull) {
          if(point.x < UVx || UVx == 0) UVx = point.x;
          if(point.y < UVy || UVy == 0) UVy = point.y;
          if(point.x > UVw) UVw == point.x;
          if(point.y > UVh) UVh == point.y;
        }
        UVw -= UVx;
        UVh -= UVy;
      }
      p.redraw();
      return;
    }
    if (leftOf(currentVertex, nextVertex, checking)) {
      nextVertex = checking;
      nextIndex = index;
    }
    index++;
    if (index == points.length && nextVertex != leftMost) {
      UVhull.push(nextVertex);
      currentVertex = nextVertex;
      index = 0;
      points = points.filter((point) => leftOf(currentVertex, leftMost, point));
      nextVertex = leftMost;
    }
  }

}

};


// Layer

let svgSelectedLayer;
let svgTrashx, svgTrashy;
let svgRotatex, svgRotatey;
let svgScalex, svgScaley;
let svgIconSize = 40;

class SvgLayer {
  
  constructor(image, layerno, layermode) {

    //show subdesigner tool to design the knife
    jQuery('#svgcontainer').show();
    
    console.log("Layer Start");
    console.log(image);
    console.log(layerno);
    console.log(layermode);
    console.log("Layer end");

    this.mode = layermode;
    this.side = currentSide;

    this.layerNumber = layerno;
    this.selected = false;
    this.image = image;

    this.x = p5s_svg_svg.width/2;
    this.y = p5s_svg_svg.height/2;
    this.aspectR = this.image.width/this.image.height;

    this.w = 700;
    this.h =  this.w/this.aspectR;
    while(this.h > p5s_svg_svg.height || this.w > p5s_svg_svg.width){
      this.w--;
      this.h =  this.w/this.aspectR;
    }

    switch (this.mode){
      case 0:
      // console.log("layer with layermode 0");
      break;
      case 1:
      // console.log("layer with layermode 1");
      break;
      case 2:
      // console.log("layer with layermode 2");
      break;
      default:
      // console.log("layer with unkown layermode");
      break;
    }


    this.slider = p5s_svg_svg.createSlider(10,90,50,1);
    this.slider.style('width', '300px');
    this.slider.position(0,210);
    this.slider.center('horizontal');
    this.slider.input(function(){
      p5s_svg_svg.redraw();
    });

    this.dragging = false;
    this.rotating = false;
    this.scaling = false;
  }

  hover(){
    if(currentSide == this.side && p5s_svg.mouseX > this.x - this.w/2 && p5s_svg.mouseX < this.x + this.w/2 && p5s_svg.mouseY > this.y - this.h/2 && p5s_svg.mouseY < this.y + this.h/2){
      return true;
    } else {
      return false;
    }
  }

  select(){
    this.selected = true;
    this.slider.show();
    this.updateButtonCoords();
    for(var i = 0; i < svglayer.length; i++){
      svglayer[i].layerNumber = i;
    }
    svgSelectedLayer = svglayer.indexOf(this);
  }

  deselect(){
    this.selected = false;
    this.slider.hide();
    this.rotating = false;
    this.scaling = false;
    this.dragging = false;
  }



  updateButtonCoords(){
    svgTrashx = this.x + this.w/2;
    svgTrashy = this.y - this.h/2;
    svgRotatex = this.x - this.w/2;
    svgRotatey = this.y + this.h/2;
    svgScalex = this.x - this.w/2;
    svgScaley = this.y - this.h/2;
  }

  drag(){
    this.x += p5s_svg.mouseX - svgmcx;
    this.y += p5s_svg.mouseY - mcy;
    svgmcx = p5s_svg.mouseX;
    mcy = p5s_svg.mouseY;
    this.updateButtonCoords();
    console.log("Drag...");
  }
  scale(){
     console.log("scaling...");
    //var scale = 1;
    /*
    this.w -= (p5s_svg.mouseX - svgmcx)*2;
    this.h -= (p5s_svg.mouseY - mcy)*2;
    svgmcx = p5s_svg.mouseX;
    mcy = p5s_svg.mouseY;*/

    var changeFactor = (p5s_svg.mouseX + p5s_svg.mouseY) - (svgmcx + mcy);
    this.w -= changeFactor * this.aspectR;
    this.h -= changeFactor;
    svgmcx = p5s_svg.mouseX;
    mcy = p5s_svg.mouseY;
    console.log(svgmcx);
    console.log(mcy);
    this.updateButtonCoords();
  }
  rotate(){
    console.log("rotating...");

    p.translate(this.w / 2, this.h / 2);
    p.rotate(100 / 3.0);
    p.rect(-26, -26, 52, 52);

    this.updateButtonCoords();
  }


  trash(){
    this.deselect();
    svglayer.splice(svgSelectedLayer,1);
    if(svglayer[svglayer.length-1] != undefined){
      svglayer[svglayer.length-1].select();
    }
    console.log("Trash the selecte image .......");

  }

  renderGfx(){
    renderSvgGraphics.image(this.image,this.x,this.y,this.w-6,this.h-6);
    renderSvgGraphics.filter(p5s_svg.THRESHOLD,this.slider.value()/100);

    console.log("renderGfx......");
  }

  renderOverlays() {
    if(this.selected){

    

      //this.updateButtonCoords();
      p5s_svg.noFill();
      //p5s_svg.stroke(230,230,230);
      p5s_svg.stroke(230,230,230);
      p5s_svg.strokeWeight(4);
      p5s_svg.rect(this.x - this.w/2, this.y - this.h/2,this.w,this.h);
      p5s_svg.fill(50,50,50);
      p5s_svg.noStroke();
      
      p5s_svg.rect(this.x - this.w/2-20,this.y - this.h/2-20,40,40);
      p5s_svg.image(svgscaleicon,svgScalex,svgScaley);

      

      p5s_svg.rect(this.x + this.w/2-20,this.y - this.h/2-20,40,40);
      p5s_svg.image(svgtrashicon,svgTrashx,svgTrashy);

      //p5s_svg.rect(this.x - this.w/2-20,this.y + this.h/2-20,40,40);
      //p5s_svg.image(svgrotateicon,svgRotatex,svgRotatey);
    }
    console.log("renderOverlays......");
  }
}




