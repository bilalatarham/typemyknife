var knife;
var knifeUrl = '';
var knifeHovering = false;
class Knife {
    static load(id) {
       
        knife = '';
        if (mainMenuOpen) {
            mainMenu.toggle();
        }
        jQuery.ajax({
            url: home_page_ur+'/wp-content/themes/flatsome-child/configurator/get.php?knife=' + id,
            type: 'post',
            data: {
                id: id
            },
            success: function (response) {
                knife = JSON.parse(response);
                console.log(knife);
                console.log("knife reponse ......");
                //console.log(knife.eigenschaften);
                if (!(knife.link3d === knifeUrl)) {
                    if (knifeUrl != '') Knife.unload();
                    knifeUrl = knife.link3d;
                    console.log(knifeUrl,"knifeUrl........")
                    app.appendScene(knifeUrl, function () {
                        jQuery('#name h1').html(knife.name);
                        jQuery('#preis h3').html(knife.preis + ",- €");
                        var mask = getMask();
                        p5s.getHull(mask.geometry.attributes.uv.array);
                    }, null, null, false, false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // console.log(textStatus, errorThrown);
            }
        });
    }

    static unload() {
        //knifeUrl = '';
        const appendedScene = app.scene.children.find(attr => attr.name == 'Scene');
        // console.log(appendedScene);
        var appendedMeshs = appendedScene.children.filter(attr => attr.type == 'Mesh');
        const appendedGroups = appendedScene.children.filter(attr => attr.type == 'Group');
        appendedGroups.forEach(function (group) {
            var meshingroup = group.children.filter(attr => attr.type == 'Mesh');
            meshingroup.forEach(function (mesh) {
                appendedScene.remove(mesh);
                mesh.geometry.dispose();
                mesh.material.dispose();
                mesh = undefined;

            });
        });
        appendedMeshs.forEach(function (mesh, i, arr) {

            appendedScene.remove(mesh);
            mesh.geometry.dispose();
            mesh.material.dispose();
            mesh = undefined;
        });
        app.scene.remove(appendedScene);
        var index = app.scene.children.indexOf(appendedScene);
        app.scene.children.splice(1, index);
        appendedScene.dispose();
    }

    static switch (id) {
        //animation ...
        if (mainMenuOpen) {
            mainMenu.toggle();
        }
        console.log("SWitch side mainMenu.toggle();");
        Knife.load(id);
    }

    static switchSide() {
        let save_svg = new Promise((myResolve, myReject) => {
            p5s.saveSVG();
            setTimeout(() => {
                myResolve("asd");
            }, 2500);
        })
        save_svg.then(function(value) {
            p5s.switchSide();
            if (currentSide) {
                tweenCamera('FrontPosEmpty', 'Empty', 2, function () {
                    //console.log("tweensomething?");
                });
            } else {
                tweenCamera('BackPosEmpty', 'Empty', 2, function () {});
            }
        });
    }

    savesvgbeforeswitch()
    {
        
    }

}