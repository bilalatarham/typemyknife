
//P5

/*
let gui = function(){
let confi = function() {
alert("yo");
};
};
*/
/*
class gui{
  static testfunction(){
    return("testvalue");
  };
  static class confi extends gui {
    static toggle(){
      alert("hi");
      //alert(gui.testfunction());
      //alert(gui.teststring);
    }
  }
};

*/


/*
class gui{
constructor(){
let confi = function(){

}
}
}

class confi extends gui {
static toggle(){
alert("toggling");
}
}
*/






var canvas;
let renderGraphics;
let textRenderGraphics;
let rotate_degree = 0;
//confiUrl = knifeUrl;
let mcx = 0;
let mxy = 0;
let layer = [];
let textlayer = [];
let rotateicon, scaleicon, trashicon;
let currentSide = true;
let scene1 = true;
//let SVG;

let UVhull = [];
let invert_image = false;

let angle = 0;

let sketch = function(p) {
    var img, imageBrowse, imageBtn, textBtn, getVecBtn;
    //let UVhull = [];
    let UVcoords = [];
    let UVmedian;

    p.convertImgToBase64 = function (url, callback, outputFormat){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image;
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
            canvas.height = img.height;
            canvas.width = img.width;
              ctx.drawImage(img,0,0);
              var dataURL = canvas.toDataURL(outputFormat || 'image/png');
              callback.call(this, dataURL);
            // Clean up
              canvas = null; 
        };
        img.src = url;
    }
    
    
 
    p.preload = function(){
        rotateicon = p.loadImage(home_page_ur+'/wp-content/themes/flatsome-child/configurator/img/rotate40x40.png');
        trashicon = p.loadImage(home_page_ur+'/wp-content/themes/flatsome-child/configurator/img/trash40x40.png');
        scaleicon = p.loadImage(home_page_ur+'/wp-content/themes/flatsome-child/configurator/img/scale40x40.png');
    }
    p.setup = function()
    {
        renderGraphics = p.createGraphics(1024,512 );
        //renderGraphics = p.createCanvas(1024,512 ,p.WEBGL);
        renderGraphics.imageMode(p.CENTER);
        renderGraphics.blendMode(p.MULTIPLY);

        imageBrowse = p.createFileInput(function(file) {
            p.loadImage(file.data, img => {
                console.log(file);
                // if(file.size < 1048576)
                // {
                //     alert('File size is too low to upload.');
                //     return false;
                // }if(file.size > 3145728)
                // {
                //     alert('Files size is too big to upload.');
                //     return false;
                // }
                layer.push(new Layer(img,layer.length,1));
                for(var i = layer.length-1; i > -1; i--) {
                    layer[i].deselect();
                }
                layer[layer.length-1].select();
                p.redraw();
            });
        },function(){
            //layer[layer.length-1].select();
        });
        imageBrowse.id("imageBrowse");
        //imageBtn.position(0,210);
        imageBrowse.hide();
        //imageBtn.center('horizontal');
        /*
        imageBtn = p.createButton('Bild hinzufügen');
        imageBtn.position(-250, 0);
        imageBtn.size(300,);
        imageBtn.id("imageButton");
        jQuery("#imageButton").click(function () {
            jQuery("#imageBrowse").trigger('click');
        });

        var switchSceneBtn = p.createButton('Messer wechseln');
        switchSceneBtn.position(-250, 60);
        switchSceneBtn.size(300,50);
        switchSceneBtn.mousePressed(toggleShop);
        //switchSceneBtn.mousePressed(switchKnife);

        getVecBtn = p.createButton('Vector speichern');
        getVecBtn.position(-250, 120);
        getVecBtn.size(300,50);
        getVecBtn.mousePressed(p.saveSVG);

        var switchSideBtn = p.createButton('Seite wechseln');
        switchSideBtn.position(-250, 180);
        switchSideBtn.size(300,50);
        switchSideBtn.mousePressed(function(){
        p.switchSide();
        if(currentSide){
            tweenCamera('FrontPosEmpty', 'Empty', 2, function() {
                console.log("tweensomething?");
            });
        } else {
            tweenCamera('BackPosEmpty', 'Empty', 2, function() {});
        }
        }
        );
        */
        /*
        //jQuery('#imageButton').css("left","0").value("yo");
        textBtn = p.createButton('Text hinzufügen');
        textBtn.position(420, 50);
        textBtn.mousePressed(p.addText);
        switchSceneBtn = p.createButton('Messer wechseln');
        switchSceneBtn.position(420, 80);
        switchSceneBtn.mousePressed(p.switchScene);
        priceBtn = p.createButton('Preis berechnen');
        priceBtn.position(420, 110);
        priceBtn.mousePressed(p.calcPrice);
        */

        // textBtn = p.createButton('Text hinzufügen');
        // textBtn.position(420, 50);
        // textBtn.mousePressed(p.addText);

        //switchSceneBtn = p.createButton('Messer wechseln');
        //switchSceneBtn.position(420, 80);
        //switchSceneBtn.mousePressed(p.switchScene);

        canvas = p.createCanvas(1024,512);
        canvas.id('p5canvas');

        //jQuery('#p5canvas').css("width","400px").css("height","200px").css("left","250px").css("top","500px"); //calc(50% - 400px/2)

        jQuery('#p5canvas').css("width","400px").css("height","200px"); //calc(50% - 400px/2)

    
        init3D(confiUrl);

        
        //switchScene(confiUrl);

        p.imageMode(p.CENTER);

        //p.addText();
        //switchScene('https://typemyknife.de/wip2/wp-content/uploads/2020/02/DICK-Premier-Plus-Kochmesser-21-cm.glb.xz');
    }

    p.switchSide = function() {

        for(var i = 0; i < layer.length; i++){
            // console.log(layer[i].side);
        }
        currentSide = !currentSide;

        var mask = getMask();
        p.getHull(mask.geometry.attributes.uv.array);

        for(var i = 0; i < layer.length; i++) {
            if(layer[i].side == currentSide) {
                for(var j = i-1; j > -1; j--) {
                    layer[j].deselect();
                }
                layer[i].select();
            } else {
                layer[i].deselect();
            }
        }
        // console.log(currentSide);
    }

    p.calcPrice = function() {
        
        // let blackPixels = p5render.getBlackPixelCount();
        // let klingenlaenge = 21; // !!!
        // alert("Schwarzflächenanteil mal Klingenlänge plus 40 .. " + (40+(Math.trunc(blackPixels/20000*klingenlaenge/7.77))));
    }

    var textBox = '';
    var canvasss = '';
    p.openTextBlock = function ()
    {
        jQuery('.text-block-d').show();
    }

    p.addText = function() {
        
        jQuery('#main-menu-frame').hide();

        //text block and canvas
        //jQuery('#canvastext').hide();
        //jQuery('#textCanvas').hide();

        jQuery('.text-block-d').hide();
        //jQuery('#p5canvas').css("width","400px").css("height","200px");

        jQuery('#p5container').show();

        //p.fill(189);
        p.stroke(255, 0, 0);
        //p.background(127);
        p.textSize(10);
        
        let get_v3d_text = jQuery('#v3d_text').val();
        let get_v3d_font_family = jQuery('#v3d_font_family').val();
        let get_v3d_font_size = jQuery('#v3d_font_size').val();
        let v3d_font_weight = jQuery('#v3d_font_weight').val();
        
        if(jQuery('#canvastext').length > 0)
        {
            jQuery('#canvastext').remove();
        }
        if(jQuery('#textCanvas').length > 0)
        {
            jQuery('#textCanvas').remove();
        }

        let canvasdiv = document.createElement("div");
        canvasdiv.id = 'canvastext';
        document.body.appendChild(canvasdiv);
        jQuery('#canvastext').text(get_v3d_text);
        jQuery('#canvastext').css({
            "fontSize":get_v3d_font_size,
            "color":"#000",
            "font-family":get_v3d_font_family,"display":"initial",
            "font-weight":v3d_font_weight
        });

        var canv = document.createElement('canvas');
            canv.id = 'textCanvas';
            canv.width = jQuery('#canvastext').width()+5;
            canv.height = jQuery('#canvastext').height()+2;
            document.body.appendChild(canv); // adds the canvas to the body element
    
            var ctx=canv.getContext("2d");
            ctx.font=get_v3d_font_size + ' '+get_v3d_font_family;//"20px Helvetica";
            ctx.fillText(get_v3d_text,0, 20);
            //ctx.textAlign = "center";
            //var canvas = document.getElementById("textCanvas");
            var img    = canv.toDataURL("image/png");
    
            // var base64data = '';
            // let node = document.getElementById('canvastext');
            // node.innerHTML = "Im an image now."
            // domtoimage.toBlob(document.getElementById('canvastext'))
            // .then(function(blob) {
            //     let reader = new FileReader();
            //         reader.readAsDataURL(blob); 
            //         reader.onloadend = function() {
            //         base64data = reader.result;                
            //         console.log(base64data);
            //     }
            // });
    
            // var img  = base64data;
    
            let draw_text = [];
            draw_text['text'] = get_v3d_text;
            draw_text['text_font_family'] = get_v3d_font_family;
            draw_text['text_font_size'] = get_v3d_font_size;
            //draw_text['text_font_weight'] = v3d_font_weight;
            console.log(img);
            console.log("TEXT IMAGE");
            p.imageLayer(img,draw_text);
            p.redraw();
        
    }

    p.invertImage = function() {
        invert_image = true;
        for(var i = 0; i < layer.length; i++) {
            if(layer[i].side == currentSide) {
                if(layer[i].selected == true) {
                    layer[i].invert_type = true;
                }
                layer[i].applyInvert();
            }
        }
        setTimeout(function() {
            console.log( jQuery("#Addinvert"));
            jQuery("#Addinvert").trigger('click');
        },3000);

        p.redraw();
    }

    p.imageLayer = function (imageData, draw_text = []) {
        p.loadImage(imageData, img => {
            layer.push(new Layer(img,layer.length,2,draw_text));
            for(var i = layer.length-1; i > -1; i--) {
                layer[i].deselect();
            }
            layer[layer.length-1].select();
            p.redraw();
        });
    
        jQuery('#imageBrowse').val(null);

        // if(hideELement)
        // {
        //     $(hideELement).hide();
        // }
    }

    p.imageGalleryLayer = function (imageUrl) {
        p.convertImgToBase64(imageUrl, function(base64Img) {
            p.loadImage(base64Img, img => {
                layer.push(new Layer(img,layer.length,1));
                for(let i = layer.length-1; i > -1; i--) {
                    layer[i].deselect();
                }
                layer[layer.length-1].select();
                p.redraw();
            });
        });
    }


    p.saveSVG = function()
    {
        jQuery('.load-d').show();
        for(let i=0; i<2;i++)
        {
            // if(i==1)
            // {
            //     p.switchSide();
            // }
            if(currentSide == true || currentSide == 'true')
            {
                console.log("Front Side");
                if(jQuery('#svgfrontside').length > 0) {
                    jQuery('#svgfrontside').html('');
                }
                //p5render.save();
               
                let canvass = jQuery('#rendercanvas')[0];
                let data = canvass.toDataURL('image/jpeg');

                // let f_svg = '';
                // f_svg += '<div><svg version="1.1" width="1024" height="512" viewBox="0 0 1024 512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs></defs><g></g>';
                // f_svg += '<g transform="">';
                // f_svg += '<image x="0" y="0" width="1024" height="512" preserveAspectRatio="none" xlink:href="'+data+'"></image>';
                // f_svg += '</g>';
                // f_svg += '</svg></div>';

                let f_jpg = '';
                f_jpg += '<div>' + data + '</div>';
                jQuery('#svgfrontside').hide();
                jQuery('#svgfrontside').html(f_jpg);
                jQuery('.load-front-img').show();
                //p5s_svg = new p5(sketchFrontSideSVG,'svgfrontside');
            }
            else
            {
                if(jQuery('#svgbackside').length > 0) {
                    jQuery('#svgbackside').html('');
                }

                let canvas = jQuery('#rendercanvas')[0];
                let data = canvas.toDataURL('image/jpeg');

                // let f_svg = '';
                // f_svg += '<div><svg version="1.1" width="1024" height="512" viewBox="0 0 1024 512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs></defs><g></g>';
                // f_svg += '<g transform="">';
                // f_svg += '<image x="0" y="0" width="1024" height="512" preserveAspectRatio="none" xlink:href="'+data+'"></image>';
                // f_svg += '</g>';
                // f_svg += '</svg></div>';
                let b_jpg = '';
                b_jpg += '<div>' + data + '</div>';
                jQuery('#svgbackside').hide();
                jQuery('#svgbackside').html(b_jpg);
                jQuery('.load-back-img').show();
                //p5s_backside_svg = new p5(sketchBackSideSVG,'svgbackside');
            }
        }
        setTimeout(function() {
            let v3d_selected_product = jQuery('#v3d_selected_product').val(v3d_product_id);
            let backendvector;
            if(jQuery('body').find('#svgbackside > div').html()) {
                backendvector = jQuery('body').find('#svgbackside > div').html();
            }

            let one_side_price_cart,sec_side_price_cart=0; 
            if(firstside_price ) {
                one_side_price_cart = parseInt(one_side_price);
            }
            if(secside_price ) {
                sec_side_price_cart = parseInt(sec_side_price);
            }

            var data = {
                'action': 'save_vector_file',
                'frontvector': jQuery('body').find('#svgfrontside > div').html(),
                'backendvector': backendvector,
                'product_id':v3d_selected_product.val(),
                'quantity_prod': 1,
                'guid':v3d_GUID,
                'one_side_price_cart':one_side_price_cart,
                'sec_side_price_cart':sec_side_price_cart
            };
            jQuery.post(ve3_product_ajax.ajaxurl, data, function(response) {
                console.log(response);
                jQuery('.load-d').hide();
            });
        }, 3500);

        //p.save('download.svg');
        //p.redraw();

        //p5s.noLoop();
        //p5render.saveScreenshot();
    }

    //  p.switchScene = function(){
    //  switchKnife(knifeUrl);
    /*
    let url;
    if(scene1){
    url = 'https://www.typemyknife.de/test5.glb.xz';
    } else {
    url = 'https://www.typemyknife.de/wip2/wp-content/uploads/2020/02/DICK-1778-Chinesisches-Kochmesser.glb.xz';
    }
    var path = url.replace(/^.*\/\/[^\/]+/, '');
    switchScene(path);
    scene1 = !scene1;
    */
    //}


    p.draw = function() {

        //p.background(127);
        //renderGraphics.background(255);
        console.log("Create Canvas .......");
        //p.clear();
        jQuery("#p5canvas")[0].getContext('2d').clearRect(0,0,p.width,p.height);

        if(UVhull != '') {
            renderGraphics.clear();
            renderGraphics.beginShape();
            renderGraphics.noStroke();
            renderGraphics.fill(255);
            for (let point of UVhull) {
                renderGraphics.vertex(point.x, point.y);
            }
            renderGraphics.endShape(p.CLOSE);

            for(var i = 0; i < layer.length; i++) {
                if(layer[i].side == currentSide) {
                    //p.rotate();
                    layer[i].renderGfx();
                    if(layer[i].invert_type == true)
                    {
                        layer[i].applyInvert();
                    }
                }
            }
        }
    
        p5render.graphics(renderGraphics);

        updateMask();
        app.render();
        p.imageMode(p.CENTER);
        p.image(renderGraphics,p.width/2,p.height/2);
        p.fill(5,10,0,50);
        //p.rect(0,0,p.width,p.height);

        if(UVhull != '') {

            p.beginShape();
            p.noStroke();
            p.fill(0,0,0,80);
            p.vertex(0,0);
            p.vertex(0,p.height);
            p.vertex(p.width,p.height);
            p.vertex(p.width,0);
            p.beginContour();
            for (let point of UVhull) {
                p.vertex(point.x, point.y);
            }
            p.endContour();
            p.endShape(p.CLOSE);

            p5render.UVcutout(UVhull);
            //p5s_svg.UVcutout(UVhull);
        }
        
        firstside_price = 0;
        secside_price = 0;
        for(var i = 0; i < layer.length; i++) {
            if(layer[i].side == currentSide) {
                //layer[i].rotate();
                layer[i].renderOverlays();
                if(layer[i].invert_type == true) {
                    layer[i].applyInvert();
                }
            }
            if(layer[i].side == true) {
                firstside_price = 1;
            } else {
                secside_price = 1;
            }
        }

        knife_price_calc = 0;
        if(firstside_price ) {
            knife_price_calc = knife_price_calc + parseInt(one_side_price);
        }
        if(secside_price ) {
            knife_price_calc = knife_price_calc + parseInt(sec_side_price);
        }
        
        if(knife_price_calc > 0) {
            if(jQuery('#configprice').length > 0) {
                jQuery('#configprice').remove();
            }
            knife_position = p.createElement('h2', 'Design Price: EUR '+knife_price_calc);
            knife_position.id('configprice');
            knife_position.position(420, 80);

            if(jQuery('#svgfrontside').html().length > 0 ||  jQuery('#svgbackside').html().length > 0)
            {
                // if(jQuery('.view-designed-knife').length > 0) {
                //     jQuery('.view-designed-knife').remove();
                // }

                // let link = p.createA(home_page_ur+"/wp-content/uploads/tmkconfigurator/pdf/"+v3d_GUID+".pdf", 
                // "View Design", "_blank");
                // //link.position(120, 80);
                // link.addClass('view-designed-knife');
            }
        }
        else
        {
            if(jQuery('#configprice').length > 0) {
                jQuery('#configprice').remove();
            }
            
            // if(jQuery('.view-designed-knife').length > 0) {
            //     jQuery('.view-designed-knife').remove();
            // }
        }

        p.rotate(0);
        p5s.rotate(0);

        p.noLoop();
    }
    p.mouseClicked = function() {
        
        //let m_angle = Math.atan2(p.mouseY - 150, p.mouseX - 150);
        //angle = /* MAGIC FORMULA HERE */ m_angle;

        // m_angle = p.atan2(p.mouseY - 150, p.mouseX - 150);
        // angle = /* MAGIC FORMULA HERE */ m_angle;
        // console.log(angle);
        // p.redraw();
    }

    p.mouseDragged = function() {

        if (p.pressedInside && p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0) {
            for(var i = 0; i < layer.length; i++) {
                
                if(layer[i].selected) {
                    if(layer[i].scaling) {
                        layer[i].scale();
                    } else if (layer[i].rotating) {
                       // alert("adsad");
                        //layer[i].rotate();
                    } else {
                        layer[i].drag();
                    }
                    p.redraw();
                    return;
                }
            }
        }
    }

    p.mouseReleased = function() {

        for (var i = 0; i < layer.length; i++) {
            layer[i].rotating = false;
            layer[i].dragging = false;
            layer[i].scaling = false;
        }
        //if(cameraSide != currentSide){
        //  p.switchSide();
        //}
        console.log("Mouse Released");
        p.pressedInside = false;
        
       p.redraw();
       
    }

    var pressedInside = false;
    p.mousePressed = function(event) {
        
        console.log('X:'+p.mouseX);
        console.log('Y:'+p.mouseY);

        // if(jQuery(event.target).hasClass('rotate-val-d')) {
        //     // alert(jQuery(event.target).text());
        //     layer[selectedLayer].rotateDegree = jQuery(event.target).text();
        //     p.redraw();
        // }

        if (p.mouseX <= p.width && p.mouseX >= 0 && p.mouseY <= p.height && p.mouseY >= 0 && jQuery('#p5container').is(':visible')) {

            p.pressedInside = true;
            mcx = p.mouseX;
            mcy = p.mouseY;
            /*
            for(var i = layer.length-1; i > -1; i--){
            if(layer[i].hover() == true && layer[i].selected == false){
            layer[i].select();
            //layer.push(layer.splice(i, 1)[0]);
            return;
            }*/

            if(layer[layer.length-1] != undefined && layer[layer.length-1].selected){
                // console.log("have a selected layer");
            } else {
                //console.log("no selected layer");
            }

            if(p.mouseX > scalex-20 && p.mouseX < scalex+20 && p.mouseY > scaley-20 && p.mouseY < scaley+20) {
                layer[selectedLayer].scaling = true;
            }

            if(p.mouseX > rotatex-20 && p.mouseX < rotatex+20 && p.mouseY > rotatey-20 && p.mouseY < rotatey+20) {
                layer[selectedLayer].rotating = true;
            }

            if(p.mouseX > trashx-20 && p.mouseX < trashx+20 && p.mouseY > trashy-20 && p.mouseY < trashy+20) {
                layer[selectedLayer].trash();
                for(var i = 0; i < layer.length; i++) {
                    layer[i].layerNumber = i;
                }
            }

            if(layer[selectedLayer] != undefined) {
                if(!layer[selectedLayer].scaling && !layer[selectedLayer].rotating) {
                    for(var i = layer.length-1; i > -1; i--) {
                        layer[i].deselect();
                        if(layer[i].hover()) {
                            for(var j = i-1; j > -1; j--) {
                                layer[j].deselect();
                            }

                            layer.push(layer.splice(i, 1)[0]);
                            layer[layer.length-1].select();
                            return;
                        }
                    }
                }
            }
            p.redraw();
        }

        if(jQuery(event.target).hasClass('rotate-val-d')) {
            //alert(jQuery(event.target).text());
            let check_layer_rotate = 0;
            if(layer[selectedLayer].rotateDegree == 0){
                check_layer_rotate = 90;
            } else if(layer[selectedLayer].rotateDegree == 90) {
                check_layer_rotate = 180;
            } else if(layer[selectedLayer].rotateDegree == 180) {
                check_layer_rotate = 270;
            } else if(layer[selectedLayer].rotateDegree == 270) {
                check_layer_rotate = 0;
            }
            layer[selectedLayer].rotateDegree = check_layer_rotate;
           p.redraw();
        }

        //rotate-val-d
        
    }



    p.getHull = function(arr) {
        var points = [];
        for (let i = 0; i < arr.length; i+=2) {
            points.push(p.createVector(p.map(arr[i],0,1,0,p.width),p.map(arr[i+1],0,1,0,p.height)));
        }
        points.sort((a, b) => a.x - b.x);
        leftOf = function(v1, v2, p) {
            const cross = p5.Vector.sub(v2, v1).cross(p5.Vector.sub(p, v1));
            return cross.z < 0;
        }
        let leftMost = points[0];
        let currentVertex = leftMost;
        UVhull = [];
        UVhull.push(currentVertex);
        let nextVertex = points[1];
        let index = 2;
        let nextIndex = -1;
        UVmedian = p.createVector(0,0);
        while (true) {
            let checking = points[index];
            if (checking == undefined) {
                if(UVhull != []) {
                    for(var i = 0; i < UVhull.length; i++) {
                        UVmedian = p5.Vector.add(UVmedian,UVhull[i]);
                    }
                    UVmedian.x = UVmedian.x / UVhull.length;
                    UVmedian.y = UVmedian.y / UVhull.length;
                    let UVx = 0;
                    let UVy = 0;
                    let UVw = 0;
                    let UVh = 0;
                    for (let point of UVhull) {
                        if(point.x < UVx || UVx == 0) UVx = point.x;
                        if(point.y < UVy || UVy == 0) UVy = point.y;
                        if(point.x > UVw) UVw == point.x;
                        if(point.y > UVh) UVh == point.y;
                    }
                    UVw -= UVx;
                    UVh -= UVy;
                }
                p.redraw();
                return;
            }
            
            if (leftOf(currentVertex, nextVertex, checking)) {
                nextVertex = checking;
                nextIndex = index;
            }
            index++;
            
            if (index == points.length && nextVertex != leftMost) {
                UVhull.push(nextVertex);
                currentVertex = nextVertex;
                index = 0;
                points = points.filter((point) => leftOf(currentVertex, leftMost, point));
                nextVertex = leftMost;
            }
        }
    }

};


// Layer
let selectedLayer;
let trashx, trashy;
let rotatex, rotatey;
let scalex, scaley;
let iconSize = 40;

class Layer {

    constructor(image, layerno, layermode ,text_layer = []) {

        jQuery('#p5container').show();

        this.text_layer = text_layer;
        this.mode = layermode;
        this.side = currentSide;
        this.invert_type = false;
        this.layerNumber = layerno;
        this.selected = false;
        this.image = image;

        this.x = p5s.width/2;
        this.y = p5s.height/2;
        this.aspectR = this.image.width/this.image.height;
        
        this.LayermouseX = 0;
        this.LayermouseY = 0;

        this.w = 700;
        this.h =  this.w/this.aspectR;
        while(this.h > p5s.height || this.w > p5s.width) {
            this.w--;
            this.h =  this.w/this.aspectR;
        }

        this.rotateDegree = 0;

        switch (this.mode) {
            case 0:
            // console.log("layer with layermode 0");
            break;
            case 1:
            // console.log("layer with layermode 1");
            break;
            case 2:
            // console.log("layer with layermode 2");
            break;
            default:
            // console.log("layer with unkown layermode");
            break;
        }

        this.slider = p5s.createSlider(10,90,50,1);
        this.slider.style('width', '300px');
        this.slider.position(0,210);
        this.slider.center('horizontal');
        this.slider.input(function() {
            p5s.redraw();
        });

        // this.invert = p5s.createButton("Add Invert filter"); 
        // this.invert.id("Addinvert");
        // this.invert.position(160, 360); 
        // this.invert.mousePressed(this.applyInvert);
        // this.invert.mousePressed(function() {
        //     this.invert_type = true;
        // });

        this.dragging = false;
        this.rotating = false;
        this.scaling = false;
    }

    hover() {

        if(currentSide == this.side && p5s.mouseX > this.x - this.w/2 && p5s.mouseX < this.x + this.w/2 && p5s.mouseY > this.y - this.h/2 && p5s.mouseY < this.y + this.h/2) {
            return true;
        } else {
            return false;
        }
    }

    select() {

        jQuery('.rotate-degree-d').show();
        this.selected = true;
        this.slider.show();
        this.updateButtonCoords();
        for(var i = 0; i < layer.length; i++){
        layer[i].layerNumber = i;
        }
        selectedLayer = layer.indexOf(this);
    }

    deselect() {
        jQuery('.rotate-degree-d').hide();
        this.selected = false;
        this.slider.hide();
        this.rotating = false;
        this.scaling = false;
        this.dragging = false;
    }

    updateButtonCoords() {

        trashx = this.x + this.w/2;
        trashy = this.y - this.h/2;
        rotatex = this.x - this.w/2;
        rotatey = this.y + this.h/2;
        scalex = this.x - this.w/2;
        scaley = this.y - this.h/2;
    }

    drag() {
        
            this.x += p5s.mouseX - mcx;
            this.y += p5s.mouseY - mcy;
            mcx = p5s.mouseX;
            mcy = p5s.mouseY;
       


        this.updateButtonCoords();
        console.log("Drag...");
    }
    scale() {

        console.log("scaling...");
        //var scale = 1;
        /*
        this.w -= (p5s.mouseX - mcx)*2;
        this.h -= (p5s.mouseY - mcy)*2;
        mcx = p5s.mouseX;
        mcy = p5s.mouseY;*/
        
        var changeFactor = (p5s.mouseX + p5s.mouseY) - (mcx + mcy);
        this.w -= changeFactor * this.aspectR;
        this.h -= changeFactor;
        mcx = p5s.mouseX;
        mcy = p5s.mouseY;

        this.LayermouseX = p5s.mouseX
        this.LayermouseY = p5s.mouseY;
       
        this.updateButtonCoords();
    }

    rotate() {

        //if(this.rotating)
        //{
            // p5s.push();
            // p5s.translate(this.w / 2, this.h / 2);
            // let a = p5s.atan2(p5s.mouseY - this.h / 2, p5s.mouseX - this.w / 2);
            // p5s.rotate(a);
            

            // this.x = p5s.mouseX - this.w / 2;
            // this.y = p5s.mouseY - this.h / 2;

            // rotatex =  this.x;
            // rotatey =  this.x;
            //this.updateButtonCoords();
            //p5s.pop();
        
        //}
    }


    trash() {

        this.deselect();
        layer.splice(selectedLayer,1);
        if(layer[layer.length-1] != undefined){
            layer[layer.length-1].select();
        }
        console.log("Trash the selecte image .......");
    }

    renderGfx() {

        //if(this.rotating ==  true || this.rotating ==  'true' )
        //{
            renderGraphics.imageMode(p5s.CENTER);
            if(this.rotateDegree > 0 && this.scaling == false){
                renderGraphics.push();
                renderGraphics.angleMode(p5s.DEGREES);
                //renderGraphics.translate((this.x+this.w/2), (this.y+this.h/2));
                //renderGraphics.translate(this.w / 2, this.h / 2);
                //renderGraphics.imageMode(p5s.CENTER);
               
               // renderGraphics.translate(( (this.x - 20) + this.w / 2) ,this.y + this.h / 2);

                renderGraphics.translate(this.x  , this.y);
                renderGraphics.rotate(this.rotateDegree);
                //renderGraphics.rect(this.x, this.y, this.w, this.h);
            }
            
          
            //let a = renderGraphics.atan2(p5s.mouseY - this.h / 2, p5s.mouseX - this.w / 2);
            //let a = renderGraphics.atan2(p5s.mouseY - this.h / 2, p5s.mouseX - this.w / 2);
            //renderGraphics.rotate(a);
            
        let innv_img = this.image;
        
        if(this.invert_type != true) {
            // if(invert_image) {
            //     p5s.filter(p5s.INVERT);
            // }

            if(this.rotateDegree > 0 && this.scaling == false)
            {
                renderGraphics.image(innv_img,0,0,this.w-6,this.h-6);
            }
            else
            {
                renderGraphics.image(innv_img,this.x,this.y,this.w-6,this.h-6);
            }
            renderGraphics.filter(p5s.THRESHOLD,this.slider.value()/100);
        } else {
            if(this.selected == true) {
                innv_img.filter(p5s.INVERT);
                renderGraphics.image(innv_img,this.x,this.y,this.w-6,this.h-6);
                renderGraphics.filter(p5s.THRESHOLD,this.slider.value()/100);

                //this.image.translate(150, 150);
                //this.image.rotate(angle);
            }
        }
        
        if(this.invert_type == true) {
            if(this.selected == true) {
                this.invert_type = false;
            }
        }
         
        //if(this.rotating ==  true || this.rotating ==  'true')
        //{
            
        if(this.rotateDegree > 0 && this.scaling == false){
            renderGraphics.pop();
        }    
        
        
        //}
    }

    applyInvert() {
        if(this.selected == true) {
            this.invert_type = true;
            p5s.filter(p5s.INVERT);    
        }
        console.log("applyInvert");
    }

    rotateDegree(){
        if(this.selected == true) {
            this.invert_type = true;    
        }
    }

    renderOverlays() {

        if(this.selected) {

            this.updateButtonCoords();

            //
            if(this.rotateDegree > 0 && this.scaling == false){
    
                p5s.push();
                p5s.rectMode(p5s.CENTER);
                p5s.angleMode(p5s.DEGREES); 
               // p5s.translate(150/2, 150/2);
                p5s.translate(this.x, this.y );
                p5s.rotate(this.rotateDegree);
                //p5s.imageMode(p5s.CENTER);
            }

            p5s.noFill();
            p5s.stroke(230,230,230);
            p5s.strokeWeight(4);

            //p5s.rect(this.x - this.w/2, this.y - this.h/2,this.w,this.h);
            if(this.rotateDegree > 0 && this.scaling == false){
                p5s.rect(0 , 0,this.w,this.h);
                p5s.fill(50,50,50);
                p5s.noStroke();
            }
            else
            {
                p5s.rect(this.x - this.w/2, this.y - this.h/2,this.w,this.h);
                p5s.fill(50,50,50);
                p5s.noStroke();

            }

            //p5s.rect(this.x - this.w/2-20,this.y - this.h/2-20,40,40);
            if(this.rotateDegree < 1 ){
                p5s.rect(this.x - this.w /2-20,this.y - this.h/2-20,40,40);
                p5s.image(scaleicon,scalex,scaley);
            }

            if(this.rotateDegree < 1 ) {
                p5s.rect(this.x + this.w/2-20,this.y - this.h/2-20,40,40);
                p5s.image(trashicon,trashx,trashy);
            }
            
            //p5s.rect(this.x - this.w/2-20,this.y + this.h/2-20,40,40);
            //p5s.image(rotateicon,rotatex,rotatey);
            if(this.rotateDegree > 0 && this.scaling == false) {
               p5s.pop();
            }
        }
        console.log("renderOverlays......");
        console.log(layer);
    }
}