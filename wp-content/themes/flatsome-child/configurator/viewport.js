

'use strict';

var ctxSettings = {};
ctxSettings.alpha = true;
ctxSettings.preserveDrawingBuffer = true;
//var preloader = createCustomPreloader(initOptions.preloaderProgressCb,initOptions.preloaderEndCb);
//var preloader = new v3d.SimplePreloader({ container: 'v3dcontainer', imageURL:'drache1.jpg', imageRotationSpeed: 0 });
////app.clearBkgOnLoad = true;m
//app.renderer.setClearColor(0x000000, 0); //transparency!
//app.renderCallbacks.push(updateMask);

var raycaster = new v3d.Raycaster();
var mouse = new v3d.Vector2();

function checkIntersection() {
  raycaster.setFromCamera(mouse, app.camera);
  var intersects = raycaster.intersectObjects([app.scene], true);
  if (intersects.length > 0 && !knifeHovering) {
    knifeHovering = true;
  } else if(intersects.length == 0 && knifeHovering) {
    knifeHovering = false;
  }
}


function matGetEditableTextures(matName) {
  var mats = [v3d.SceneUtils.getMaterialByName(app, matName)];
  var textures = mats.reduce(function(texArray, mat) {
    var matTextures = Object.values(mat.nodeTextures);
    Array.prototype.push.apply(texArray, matTextures);
    return texArray;
  }, []);
  return textures.filter(function(elem) {
    return elem && (elem.constructor == v3d.Texture || elem.constructor == v3d.DataTexture);
  });
}

function updateMask() {
  if(app.scene && knifeUrl != ''){
    var tex;
    if(currentSide){
      tex = matGetEditableTextures("mask")[0];
    } else {
      tex = matGetEditableTextures("mask_back")[0];
    }
    tex.image = document.getElementById('rendercanvas');
    tex.format = v3d.RGBFormat;
    tex.needsUpdate = true;
  }
}

function getObjByName(objName){
  var objFound;
  app.scene.traverse(function(obj) {
    if (!objFound && (obj.name == objName)) {
      objFound = obj;
    }
  });
  console.log("object found: " + objFound);
  return objFound;
}

function tweenCamera(posObjName, targetObjName, duration, doSlot) {

  console.log(posObjName);
  console.log(targetObjName);
  console.log(duration);
  console.log(doSlot);
  console.log("tweening");
  duration = Math.max(0, duration);

  if (!targetObjName)
  return;
  console.log("target: " + targetObjName);
  if (posObjName)
  var posObj = getObjByName(posObjName);
  else
  var posObj = app.camera;
  console.log("habe ich position? " + posObj);
  var targetObj = getObjByName(targetObjName);
  if (!posObj || !targetObj)
  return;

  var wPos = posObj.getWorldPosition();
  var wTarget = targetObj.getWorldPosition();
  if (app.controls && app.controls.tween) {
    // orbit and flying cameras
    if (!app.controls.inTween)
    console.log("controls passen, renne tween mit wPos: " + wPos + " wTarget: " + wTarget + " duration: " + duration + " doSlot..");
    app.controls.tween(wPos, wTarget, duration, doSlot);
  } else { // TODO: static camera, just position it for now
    if (app.camera.parent)
    app.camera.parent.worldToLocal(wPos);
    app.camera.position.copy(wPos);
    app.camera.lookAt(wTarget);
    doSlot();
  }
}

function init3D(url){
  app.loadScene(url, function() {
    app.enableControls();
    app.animate = function(){
      var scope = this;


      var cb = function() {
        requestAnimationFrame(cb);

        var elapsed = scope.clock.getDelta();
        scope.elapsed = elapsed;

        if (scope.controls && !knifeHovering)
        scope.controls.update(elapsed);
      }

      cb();
    }
    app.run();


  //   let urlParams = new URLSearchParams(window.location.search);
  //   if(urlParams.has('prod_id') == true) {
  //     console.log("loadinggggggggggggggggggggggggggggggggggggggggggggg");
  //     let v3d_prod_id = urlParams.get('prod_id');
  //     Knife.load(v3d_prod_id);
  // } else {
    Knife.load('1330');
  //}

    
    //Knife.load('4126');


    runCode();
    //dof(0.0,200, 0.025, 1);
    //bloom(7.5, 0.1, 0.5);
    /* CALL P5 HERE*/
  });

 
}

var getMask = function(){
  console.log(app.scene,"App Sceneeeeeeeeeeeeeeee");
  const appendedScene = app.scene.children.find(attr => attr.name == 'Scene');
  var mask;
  console.log(appendedScene,"Append Scene");
  if(currentSide){
    mask = appendedScene.children.filter(attr => attr.name == 'mask');
    // mask = appendedScene.children[0]//.filter(attr => attr.type == 'Group').children;
     //console.log(mask,"IFFFFFFFFFFFFFFFFFFFFF");
     //mask = mask[0];
  } else {
    mask = appendedScene.children.filter(attr => attr.name == 'mask_back');
  }
  return mask[0];

}



var cameraSide;



function runCode(){
  // render the scene first time immediately after loading
  app.render();

  // use the 'change' event of the application controls, to render the scene
  // each time the camera moves
  if (app.controls) {
    app.controls.addEventListener('change', function() {
      cameraSide = (app.controls.object.position.z > 0);
      if (!app.controls.inTween){
        if(cameraSide != currentSide){
          p5s.switchSide();
        }
      }
      app.render();
    });
  }


  // viewport resizing changes the camera's aspect; re-render the scene
  window.addEventListener('resize', function() {
    app.render();
  }, false);
//  window.addEventListener('mousemove', onTouchMove);
//  window.addEventListener('touchmove', onTouchMove);
  function onTouchMove(event) {

    var x, y;

    if (event.changedTouches) {

      x = event.changedTouches[0].pageX;
      y = event.changedTouches[0].pageY;

    } else {

      x = event.clientX;
      y = event.clientY;

    }

    mouse.x = (x / window.innerWidth) * 2 - 1;
    mouse.y = - (y / window.innerHeight) * 2 + 1;

    checkIntersection();

  }
  //console.log(app);
}

function dof(focus, aperture, maxblur, depthLeakThreshold) {
  app.enablePostprocessing([{
    type: 'dof',
    focus: focus,
    aperture: aperture,
    maxblur: maxblur,
    depthLeakThreshold: depthLeakThreshold
  }]);
}
function bloom(threshold, strength, radius) {
  app.enablePostprocessing([{
    type: 'bloom',
    threshold: threshold,
    strength: strength,
    radius: radius
  }]);
}









// p5renderbuffer


let rendersketch = function(p){
  console.log("rendersketch.......");
  let rendercanvas;
  p.setup = function(){
    rendercanvas = p.createCanvas(2048,1024);
    rendercanvas.id('rendercanvas');
    jQuery('#rendercanvas').hide();

    p.noLoop();
  }
  p.graphics = function(gfx){
    p.image(gfx,0,0,p.width,p.height);
  }
  p.UVcutout = function(UVhull){

    console.log(p.width);
    console.log(p.height);
    p.beginShape();
    p.stroke(1);
    //p.fill(196,59,59);
    // p.fill(255,255,255);
    p.vertex(0,0);
    p.vertex(0,p.height);
    p.vertex(p.width,p.height);
    p.vertex(p.width,0);
    p.beginContour();
    for (let point of UVhull) {
      p.vertex(point.x*2, point.y*2);
    }
    p.endContour();
    p.endShape(p.CLOSE);

    /*
    if(serverScrs){
    //p.filter(p.BLUR,2);
    var canvas = jQuery('#rendercanvas')[0];
    var data = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
    let iname = "screenshot.png"
    jQuery.post('https://www.coalmedia.eu/knife/save.php' ,{data: data, iname});
    window.open("https://www.coalmedia.eu/knife/converter.php","_blank","menubar=1,resizable=1,width=350,height=250");
    serverScrs = false;
  }*/
}


p.saveScreenshot = function(){
  //p.filter(p.BLUR,2);
  var canvas = jQuery('#rendercanvas')[0];
  console.log(canvas.toDataURL('image/png'));
  var data = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
  let iname = "screenshot.png"
  //jQuery.post('https://www.coalmedia.eu/knife/save.php' ,{data: data, iname});
  //window.open("https://www.coalmedia.eu/knife/converter.php","_blank","menubar=1,resizable=1,width=350,height=250");

  //serverScrs = true;
}

//let serverScrs = false;

p.keyPressed = function(){
  
  if (p.keyCode === p.UP_ARROW) {
    //serverScrs = true;
  }
}

p.getBlackPixelCount = function(){
  let blackPixelCount = 0;
  p.loadPixels();
  for (let i = 0; i < p.width; i++) {
    for (let j = 0; j < p.height; j++) {
      if(p.get(i,j)[0] < 127){
        blackPixelCount++;
      }
    }
  }
  p.updatePixels();


  // return(blackPixelCount)
}
}






var rendersvg;
let sketchFrontSideSVG = function(p){
  console.log("sketchFrontSideSVG.......SVG..........sss");
  let treeImg;
  p.preload = function(){
    //treeImg = p.loadImage("http://localhost/typemyknife/burgvogel.png");
  }
  
  p.setup = function() {
    
    rendersvg = p.createCanvas(1024,512); // Create SVG Canvas
    //rendersvg = p.createCanvas(1024,512, p.SVG); // Create SVG Canvas
    rendersvg.id('rendersvg');
    
    // p.rectMode(p.CENTER);
    // p.imageMode(p.CENTER);
    
    jQuery('#rendersvg').hide();
    
    //p.strokeWeight(2); // do 0.1 for laser
    //p.stroke(255, 180, 89); // red is good for laser

    //p.fill(5,10,0,50);

    //p.noFill(); // better not to have a fill for laser
  }


  p.UVcutout = function(UVhull){
    
    p.beginShape();
    p.stroke(1);
    // p.fill(196,59,59);
    // p.fill(255,255,255);
    p.vertex(0,0);
    p.vertex(0,p.height);
    p.vertex(p.width,p.height);
    p.vertex(p.width,0);
    p.beginContour();
    for (let point of UVhull) {
        //p.vertex(point.x*2, point.y*2);
        p.vertex(point.x, point.y);
    }
    p.endContour();
    p.endShape(p.CLOSE);
  }
  
  p.draw = function() {

    // console.log(UVhull)
    // if(UVhull != '') {
    //   p.beginShape();
    //   p.noStroke();
    //   p.fill(0,0,0,80);
    //   p.vertex(0,0);
    //   p.vertex(0,p.height);
    //   p.vertex(p.width,p.height);
    //   p.vertex(p.width,0);
    //   p.beginContour();
    //   for (let point of UVhull) {
    //     p.vertex(point.x, point.y);
    //   }
    //   p.endContour();
    //   p.endShape(p.CLOSE);
  
        //p5render.UVcutout(UVhull);
    //   console.log("p5s_svg.UVcutout(UVhull).............................")
      // p5s_svg.UVcutout(UVhull);
    // }
    // 

    setTimeout(function() {
        p5s_svg.UVcutout(UVhull);
       
        for(var i = 0; i < layer.length; i++) {
            if(layer[i].side == currentSide) {
                
                let current_h = layer[i].h;
                let current_w = layer[i].w;

                var changeFactorSVG = (p5s.mouseX + p5s.mouseY) - (layer[i].x + layer[i].y);
                
                if(layer[i].rotateDegree > 0)
                {
                    
                    //p.push();
                    p5s_svg.imageMode(p.CENTER); 
                    if(layer[i].LayermouseX > 0)
                    {
                        //p.scale(layer[i].LayermouseX,layer[i].LayermouseY);
                       // p.scale(layer[i].x ,layer[i].y);
                      // p.scale(layer[i].x ,layer[i].y);
                    }
                   
                    p5s_svg.angleMode(p.DEGREES);
                    p5s_svg.translate((layer[i].x) ,(layer[i].y) );
                    p5s_svg.rotate(layer[i].rotateDegree);
                }
                else
                {
                    p.imageMode(p.CENTER);
                }
              
                if(layer[i].rotateDegree > 0 )
                {
                  p5s_svg.image(layer[i].image,layer[i].x ,layer[i].y,current_w  ,current_h);
                    // p.image(layer[i].image,0 ,0,current_w  ,current_h);
                }
                else
                {
                  p5s_svg.image(layer[i].image,layer[i].x ,layer[i].y ,current_w  ,current_h);
                }
                if(layer[i].rotateDegree > 0) {
                  //p5s_svg.pop();
                  //p5s_svg.noLoop();
                }
       
                //p.image(layer[i].image,0,0,150,150);
                //p.filter(p.THRESHOLD,layer[i].slider.value()/100);
                //p.filter(p.THRESHOLD);
                //layer[i].renderSvgGfx();
                
                //p5s_svg.save();
                //p5s_svg.noLoop();
                p5s_svg.noLoop();
            }
            //p.rotate(0);
        }
        p5s_svg.rotate(0);
        //p5s_svg.rotate(0);
        
       // p.noLoop();
    }, 1000);

    //p.rotate(0);

    // p.background(255, 180, 89);
    // p.image(treeImg, 50, 0);
    // p.image(treeImg, 50, 0,100,100);
    // p.textSize(32);
    // p.fill(0, 102, 153);
    // p.text('here is text', 50, 100,200,101);
    

    //  p.loadImage('http://localhost/typemyknife/wp-content/uploads/2020/02/Zwilling-TWIN-Cuisine-Officemesser-10-cm-768x512.jpg', img => {
    //  
    // });
    
    //p.save("mySVG.svg"); // give file name
    //p.print("saved svg");
    p.noLoop(); // we just want to export once
   
  }

  p.graphics = function(gfx){
    p.image(gfx,0,0,p.width,p.height);
  }

}

var renderbacksidesvg;
let sketchBackSideSVG = function(p) {
    p.setup = function() {
     renderbacksidesvg = p.createCanvas(1024,512, p.SVG); // Create SVG Canvas
     // renderbacksidesvg = p.createCanvas(1024,512); // Create SVG Canvas
      renderbacksidesvg.id('renderbacksidesvg');
      jQuery('#renderbacksidesvg').hide();
    }

    p.UVcutout = function(UVhull) {
        p.beginShape();
        p.stroke(1);
        // p.fill(196,59,59);
        // p.fill(255,255,255);
        p.vertex(0,0);
        p.vertex(0,p.height);
        p.vertex(p.width,p.height);
        p.vertex(p.width,0);
        p.beginContour();
        for (let point of UVhull) {
            //p.vertex(point.x*2, point.y*2);
            p.vertex(point.x, point.y);
        }
        p.endContour();
        p.endShape(p.CLOSE);
    }
  
    p.draw = function() {
        setTimeout(function() {
            p5s_backside_svg.UVcutout(UVhull);
            for(var i = 0; i < layer.length; i++) {
                if(layer[i].side == currentSide) {
                    // let current_h = layer[i].h;
                    // let current_w = layer[i].w;
                    // layer[i].image.filter(p.THRESHOLD,layer[i].slider.value()/100);
                    // p.image(layer[i].image,(layer[i].x*2),(layer[i].y*2),current_w,current_h);

                    let current_h = layer[i].h;
                    let current_w = layer[i].w;
                    p.imageMode(p.CENTER);
                    if(layer[i].rotateDegree > 0)
                    {
                        p.push();
                        //p.imageMode(p.CENTER);
                        //p.rectMode(p.CENTER);
                        p.angleMode(p.DEGREES);
                        p.translate(layer[i].x,layer[i].y);
                        p.rotate(layer[i].rotateDegree);
                    }
                    layer[i].image.filter(p.THRESHOLD,layer[i].slider.value()/100);
                    if(layer[i].rotateDegree > 0 )
                    {
                        //p.image(layer[i].image,0,0,current_w*layer[i].aspectR-6,current_h*layer[i].aspectR-6);
    
                        p.image(layer[i].image,0 ,0 ,current_w  ,current_h);
                    }
                    else
                    {
                        let ss = layer[i].x / 2;
                        p.image(layer[i].image,layer[i].x ,layer[i].y ,current_w  ,current_h);
                       // p.image(layer[i].image,200,200,150,150);
                    }
     
                    if(layer[i].rotateDegree > 0) {
                        p.pop();
                    }
    


                    //p.filter(p.THRESHOLD,layer[i].slider.value()/100);
                }
            }
            //p.save("mySVG.svg");
            p.noLoop();
        }, 400);
        p.noLoop();
    }

}
