��          L      |       �   H   �      �        A        X  +  r  l   �             A   /     q                                         Allows admins to export media library files as a compressed zip archive. Export Media Library Mass Edge Inc. https://github.com/massedge/wordpress-plugin-export-media-library https://www.massedge.com/ PO-Revision-Date: 2019-11-17 12:55:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha
Language: it
Project-Id-Version: Plugins - Export Media Library - Stable (latest release)
 Consente agli amministratori di esportare i file della libreria multimediale come un archivio zip compresso. Export Media Library Mass Edge Inc. https://github.com/massedge/wordpress-plugin-export-media-library https://www.massedge.com/ 